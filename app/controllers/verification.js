// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

Alloy.Globals.verification = null;
Alloy.Globals.verification = $;

var otpFlag = false;
/* Add require files */
var commonFile = require('common');

commonFile.createTouchEffect($.verify, "#a6ffffff", "#ffffff");
commonFile.createTouchEffect($.resend, "#a6ffffff", "#ffffff");


$.verify.backgroundColor = Ti.App.Properties.getString('appPrimaryColor')||"#CB202D";
$.resend.backgroundColor = Ti.App.Properties.getString('appPrimaryColor')||"#CB202D";

$.phoneLbl.text = "SMS is sent on\nyour number +"+args.phone;
/*
 * require header view
 */
var headerView = require('headerView').createHeaderView("verification", "VERIFICATION", {
	menu : false,
	view : true,
	backgroundimage : false
}, goToBack);
$.verificationSuperView.add(headerView);

/*
 * clear memory on back
 */
function goToBack() {

	//if(otpFlag == false){

	$.verification.animate({
		opacity : 0,
		duration : 200
	}, function() {
		setTimeout(function() {
			Ti.App.Properties.setString("openWindow", args.previousWindow);
			Alloy.Globals.verification = null;
			commonFile = null;
			args = null;
			$.verification.removeAllChildren();
			$.verification.parent.remove($.verification);

		}, 201);
	});
	//}
	// else{
	// $.mobileVerification.height = Ti.UI.SIZE;
	// $.otpVerification.height = "0dp";
	// otpFlag = false;
	// }
};

exports.goToBack = goToBack;

$.verification.animate({
	opacity : 1,
	duration : 200
});

var data = [$.mobileTxt];
$.verification.addEventListener('click', function(e) {
	commonFile.blurTextfield(e, data);
});


function resendOtp(){
	commonFile.showFullLoader($.verificationSuperView);
		var url = commonFile.loginApi;
		var params = {};
		var params = {
			name : args.name,
			mobile : args.phone,
			country_code : 91
		};

		commonFile.webServiceCall(url, JSON.stringify(params), resendOtpCallBack, "POST", $.verificationSuperView);
}

function resendOtpCallBack(response){
	if (response.status == true) {
		
		args.otp = response.data.otp;
		commonFile.hideFullLoader($.verificationSuperView);
	}else {
		commonFile.hideFullLoader($.verificationSuperView);
		commonFile.showAlert($.verificationSuperView,"Something went wrong");
	}
}


$.verify.addEventListener('click', function(e) {
	Ti.API.info('vaerifiy');
	args.callback();

	// if(otpFlag == false){
	// $.mobileVerification.height = "0dp";
	// $.otpVerification.height = Ti.UI.SIZE;
	// otpFlag = true;
	// }else{
	// $.mobileVerification.height = Ti.UI.SIZE;
	// $.otpVerification.height = "0dp";
	// otpFlag = false;
	// }
	Ti.API.info('args.otp = '+args.otp);
	Ti.API.info('$.otpTxt.getValue() = '+$.otpTxt.getValue());
	if ($.otpTxt.getValue() != args.otp) {
		commonFile.showAlert($.verificationSuperView, "Please enter valid otp");
	} else {

		commonFile.showFullLoader($.verificationSuperView);
		var url = commonFile.otpApi;
		var params = {};
		var params = {
			otp : $.otpTxt.getValue(),
			id : Ti.App.Properties.getString("userId")
		};

		commonFile.webServiceCall(url, JSON.stringify(params), otpCallBack, "POST", $.verificationSuperView);
	}
});

function otpCallBack(response) {
	Ti.API.info('response = ' + JSON.stringify(response));
	if (response.status == true) {
		args.callback();
		Alloy.Globals.leftSlider.setUserData();
		commonFile.hideFullLoader($.verificationSuperView);
		Alloy.Globals.signIn.goToBack();
		goToBack();
	} else {
		commonFile.hideFullLoader($.verificationSuperView);
		commonFile.showAlert($.verificationSuperView, "Something went wrong");
	}

}
