// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

Alloy.Globals.pastOrder = null;
Alloy.Globals.pastOrder = $;


var otpFlag = false;
/* Add require files */
var commonFile = require('common');

/*
 * require header view
 */
var headerView = require('headerView').createHeaderView("pastOrder", "PAST ORDER", {
	menu : false,
	view : true,
	backgroundimage : false
}, goToBack);
$.pastOrderSuperView.add(headerView);

/*
 * clear memory on back
 */
function goToBack() {

	$.pastOrder.animate({
		opacity : 0,
		duration : 200
	}, function() {
		setTimeout(function() {
			Ti.API.info('args.previousWindow = ' + args.previousWindow);
			Ti.App.Properties.setString("openWindow", args.previousWindow);
			Alloy.Globals.pastOrder = null;
			commonFile = null;
			$.pastOrder.removeAllChildren();
			$.pastOrder.parent.remove($.pastOrder);
			//require('navdrawer').removeWindow($.pastOrder);

		}, 201);
	});

};

exports.goToBack = goToBack;

$.pastOrder.animate({
	opacity : 1,
	duration : 200
});

function orderList() {
	commonFile.showFullLoader($.pastOrderSuperView);
	var url = commonFile.orderListApi+Ti.App.Properties.getString("userId");
	var params = {};
	// var params = {
		// user_id : Ti.App.Properties.getString("userId"),
	// };

	commonFile.webServiceCall(url, JSON.stringify(params), orderListCallBack, "GET", $.pastOrderSuperView);
}

orderList();

function orderListCallBack(response) {
	Ti.API.info('response = ' + JSON.stringify(response));

	if (response.status == true) {
		loadData(response.data);
	} else {
		commonFile.hideFullLoader($.pastOrderSuperView);
		commonFile.showAlert($.pastOrderSuperView, "Something went wrong");
	}
}


function loadData(data) {

	var listData2 = [];

	_.each(data, function(value, key) {

		listData2.push({
			properties : {
				orderID : value.id
			},
			trackingId : {
				text : "Tracking Id - "+value.id
			},
			orderTime : {
				text : value.pick_up_time+" | $"+value.total
			},
			template : 'orderTemplate',
		});

	}, commonFile.hideFullLoader($.pastOrderSuperView));

	$.orderSection.appendItems(listData2);
};


$.listView.addEventListener('itemclick', function(e) {
	var bind = e.bindId;
	var index = e.itemIndex;
	var a = e.section.items[index];

	if (e.bindId == "viewDetail") {
		var windowData = {
			previousWindow : "pastOrder",
			from : "pastOrder",
			order_id : a.properties.orderID,
		};
		Alloy.Globals.dashboard.addView(Alloy.createController('orderSummary', windowData).getView());
		Ti.App.Properties.setString("openWindow", "orderSummary");
	}

});
