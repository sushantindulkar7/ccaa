// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

Alloy.Globals.terms_condition = null;
Alloy.Globals.terms_condition = $;

/* Add require files */
var commonFile = require('common');

/*
 * require header view
 */
var headerView = require('headerView').createHeaderView("terms_condition", "TERMS & CONDITIONS", {
	menu : false,
	view : true,
	backgroundimage : false
}, goToBack);
$.termsSuperView.add(headerView);

/*
 * clear memory on back
 */
function goToBack() {
	
	$.terms_condition.animate({
		opacity : 0,
		duration : 200
	}, function() {
		setTimeout(function() {
			Ti.App.Properties.setString("openWindow",args.previousWindow);
			Alloy.Globals.terms_condition = null;
			commonFile = null;
			$.terms_condition.removeAllChildren();
			$.terms_condition.parent.remove($.terms_condition);
			//require('navdrawer').removeWindow($.contactUs);
			
		}, 201);
	});
	
	
	
};

exports.goToBack = goToBack;

$.terms_condition.animate({
	opacity : 1,
	duration : 200
});
