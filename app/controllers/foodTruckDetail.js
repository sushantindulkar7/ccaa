// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

var truckdata = args;
//Ti.API.info('args == ' + JSON.stringify(args));

var productdata = null;

Alloy.Globals.foodTruckDetail = null;
Alloy.Globals.foodTruckDetail = $;

if (Ti.App.Properties.getBool("cartFlag")) {
	$.cartLbl.visible = true;
}

/* Add require files */
var commonFile = require('common');

commonFile.createTouchEffect($.cartLbl, "#a6ffffff", "#ffffff");

$.cartLbl.text = commonFile.cartIcon + " " + commonFile.arrowRightIcon;

/*
 * require header view
 */
var headerView = require('headerView').createHeaderView("foodTruckDetail", args.truckName, {
	menu : false,
	view : true,
	rightMenu : true,
	backgroundimage : false
}, goToBack);
$.foodTruckSuperView.add(headerView);

//require('headerView').headerView.backgroundColor = "transparent";

$.cartLbl.backgroundColor = Ti.App.Properties.getString('appPrimaryColor')||"#CB202D";
$.cartLbl.borderColor = Ti.App.Properties.getString('appPrimaryColor')||"#CB202D";  


/*
 * clear memory on back
 */
function goToBack() {
	$.foodTruckDetail.animate({
		opacity : 0,
		duration : 200
	}, function() {
		setTimeout(function() {
			Ti.App.Properties.setString("openWindow", args.previousWindow);
			Alloy.Globals.foodTruckDetail = null;
			commonFile = null;
			$.foodTruckDetail.removeAllChildren();
			$.foodTruckDetail.parent.remove($.foodTruckDetail);
		}, 201);
	});
};

exports.goToBack = goToBack;

function init(data) {
	var listData = [];
	$.detailSection.appendItems(listData);
	listData.push({
		properties : {

		},
		truckName : {
			text : data.truckName,
		},
		starIcon : {
			text : commonFile.starIcon,
			color : "gray"
		},
		truckDescription : {
			text : data.truckDescription,
		},
		truckAddress : {
			text : data.truckAddress,
		},
		truckImage : {
			text : data.truckImage,
		},
		weekTime : {
			//text : "On Week Days : " + data.weekTime || "00:00 AM To 00:00 PM"
			text : (data.weekTime =="") ? "" : "On Week Days : " + data.weekTime,
			height : (data.weekTime =="") ? "0dp" : Titanium.UI.SIZE
		},
		weekEndTime : {
			//text : "On Week Ends : " + data.weekEndTime || "00:00 AM To 00:00 PM"
			text : (data.weekEndTime =="") ? "" : "On Week Ends : " + data.weekEndTime,
			height : (data.weekEndTime =="") ? "0dp" : Titanium.UI.SIZE
		},
		seperator : {
			height : (data.weekTime =="" && data.weekEndTime =="") ? "0dp" : "0.6dp"
		},
		template : 'detailTemplate',
	});
	$.detailSection.appendItems(listData);
	commonFile.hideFullLoader($.foodTruckSuperView);
}

//init(truckdata);
truckDetails();

function truckDetails() {
	commonFile.showFullLoader($.foodTruckSuperView);
	var orderId,
	    userId = null;
	if (!commonFile.isNullVal(Ti.App.Properties.getString("orderId"))) {
		orderId = Ti.App.Properties.getString("orderId");
	} else {
		orderId = 0;
	}
	if (Ti.App.Properties.getString("userId") != null || Ti.App.Properties.getString("userId") != undefined) {
		userId = Ti.App.Properties.getString("userId");
	} else {
		userId = 0;
	}
	var url = commonFile.truckInfoApi;
	var params = {};
	var params = {
		truck_id : args.truckId,
		customer_id : userId,
	};

	commonFile.webServiceCall(url, JSON.stringify(params), truckDetailCallBack, "POST", $.foodTruckSuperView);
}

function truckDetailCallBack(response) {
	Ti.API.info('response = ' + JSON.stringify(response));
	Ti.API.info('$.detailSection = ' + JSON.stringify($.detailSection));
	if (response.status == true) {
		//if (response.response.menu == false) {
		// commonFile.showAlert($.foodTruckSuperView, "Something went wrong");
		// commonFile.hideFullLoader($.foodTruckSuperView);
		// } else {
		var truckWebData = {
			truckName : response.data.truck[0].name,
			truckDescription : response.data.truck[0].description,
			truckAddress : response.data.truck[0].address,
			truckImage : response.data.truck[0].logo,
			weekTime : response.data.truck[0].weekdaytime,
			weekEndTime : response.data.truck[0].weekendtime,
			orderId : response.data.order[0].id,
		};
		Ti.App.Properties.setString("orderId", response.data.order[0].id);
		Ti.App.Properties.setString("truckId", response.data.truck[0].id);
		init(truckWebData);

		if (response.data.items.length != 0) {
			loadData(response.data.items);
		}

	} else {
		commonFile.hideFullLoader($.foodTruckSuperView);
		commonFile.showAlert($.foodTruckSuperView, "Something went wrong");
	}
}

$.foodTruckDetail.animate({
	opacity : 1,
	duration : 200
});

//$.foodScroll.addEventListener('scroll', headerEffect);

// function headerEffect(e) {
// // Ti.API.info('scroll event');
// var ycordinates = $.foodScroll.contentOffset.y;
// var xcordinates = $.foodScroll.contentOffset.x;
// Ti.API.info('ycordinates = ' + ycordinates + " xcordinates = " + xcordinates);
// if (ycordinates == 0 && xcordinates == 0) {
// // Ti.API.info('into this condtion');
// // $.explore_lbl.touchEnabled = true;
// //$.collectionContainer.height = 0;
// //$.explore_lbl.fireEvent("click");
// }
// if (ycordinates >= 0 && ycordinates < 23) {
// require('headerView').headerView.backgroundColor = "#00CB202D";
// } else if (ycordinates > 27 && ycordinates < 50) {
// require('headerView').headerView.backgroundColor = "#0dCB202D";
// } else if (ycordinates > 50 && ycordinates < 70) {
// require('headerView').headerView.backgroundColor = "#80CB202D";
// } else if (ycordinates > 70 && ycordinates < 90) {
// require('headerView').headerView.backgroundColor = "#8cCB202D";
// } else if (ycordinates > 90 && ycordinates < 110) {
// require('headerView').headerView.backgroundColor = "#99CB202D";
// } else if (ycordinates > 110 && ycordinates < 120) {
// require('headerView').headerView.backgroundColor = "#a6CB202D";
// } else if (ycordinates > 120 && ycordinates < 140) {
// require('headerView').headerView.backgroundColor = "#b3CB202D";
// } else if (ycordinates > 140 && ycordinates < 160) {
// require('headerView').headerView.backgroundColor = "#bfCB202D";
// }
//
// // else if (ycordinates > 160 && ycordinates < 180) {
// // require('headerView').headerView.backgroundColor = "#66CB202D";
// // } else if (ycordinates > 180 && ycordinates < 200) {
// // require('headerView').headerView.backgroundColor = "#73CB202D";
// // } else if (ycordinates > 200 && ycordinates < 220) {
// // require('headerView').headerView.backgroundColor = "#80CB202D";
// // } else if (ycordinates > 220 && ycordinates < 240) {
// // require('headerView').headerView.backgroundColor = "#8cCB202D";
// // } else if (ycordinates > 240 && ycordinates < 260) {
// // require('headerView').headerView.backgroundColor = "#99CB202D";
// // } else if (ycordinates > 260 && ycordinates < 280) {
// // require('headerView').headerView.backgroundColor = "#a6CB202D";
// // } else if (ycordinates > 280 && ycordinates < 300) {
// // require('headerView').headerView.backgroundColor = "#b3CB202D";
// // } else if (ycordinates > 900 && ycordinates < 920) {
// // require('headerView').headerView.backgroundColor = "#bfCB202D";
// //}
// else if (ycordinates > 160 && ycordinates < 180) {
// require('headerView').headerView.backgroundColor = "#ccCB202D";
// } else if (ycordinates > 180 && ycordinates < 200) {
// require('headerView').headerView.backgroundColor = "#d9CB202D";
// } else if (ycordinates > 200 && ycordinates < 220) {
// require('headerView').headerView.backgroundColor = "#e6CB202D";
// } else if (ycordinates > 220 && ycordinates < 240) {
// require('headerView').headerView.backgroundColor = "#f2CB202D";
// } else if (ycordinates > 240) {
// require('headerView').headerView.backgroundColor = "#ffCB202D";
// }
// //require("icon").setColor($.dashboardNavigation,ycordinates);
// }
function loadData(data) {
	var listData1 = [];
	Ti.API.info('inside========================= ');

	_.each(data, function(value, key) {
		//	Ti.API.info('value = ' + JSON.stringify(value));
		listData1.push({
			properties : {
				foodTruckId : Ti.App.Properties.getString("truckId"),
				productId : value.id
			},
			productImage : {
				image : value.img
			},
			productName : {
				text : value.name
			},
			categoryName : {
				color : Ti.App.Properties.getString('appPrimaryColor')||"#CB202D",
				text : value.category_name,
			},
			productDescription : {
				text : value.description
			},
			price : {
				text : "$" + value.price
			},
			indicationIcon : {
				text : commonFile.circleIcon,
				color : ((value.veg == 0 || value.veg == "0")? "#DA251E" : "#47d147"),
				borderColor : ((value.veg == 0 || value.veg == "0") ? "#DA251E" : "#47d147")
			},
			minus : {
				text : commonFile.minusIcon,
				price : value.price,
				qty : 1,
			},
			quantity :{
				text : 1,
				price : value.price,
			},
			plus : {
				text : commonFile.plusIcon,
				price : value.price,
				qty : 1,
			},
			quantityContainer : {
				height : "0dp"
			},
			addLbl : {
				backgroundColor : Ti.App.Properties.getString('appPrimaryColor')||"#CB202D",
				height : "25dp",
				price : value.price,
				qty : 1,
			},
			template : 'menuTemplate',
		});

	}, commonFile.hideFullLoader($.foodTruckSuperView));

	$.menuSection.appendItems(listData1);

}

$.listView.addEventListener('itemclick', function(e) {
	var bind = e.bindId;
	var index = e.itemIndex;
	var a = e.section.items[index];

	if (Ti.App.Properties.getString("userId") != null || Ti.App.Properties.getString("userId") != undefined) {
		if (e.bindId == "addLbl") {
			addToCart(e);
		}else if(e.bindId == "plus"){
			a['quantity'].text = (a['quantity'].text+1);
			e.section.updateItemAt(e.itemIndex, a);
			addToCart(e);
		}else if(e.bindId == "minus"){
			if(a['quantity'].text >1){
				a['quantity'].text = (a['quantity'].text-1);
				e.section.updateItemAt(e.itemIndex, a);
				addToCart(e);
			}
			
		}else if(e.bindId == "starIcon"){
			if(a['starIcon'].color == "gray"){
				a['starIcon'].color = commonFile.headerColor;
			}else{
				a['starIcon'].color = "gray";
			}
			e.section.updateItemAt(e.itemIndex, a);
		}
	} else {
		var windowData = {
			previousWindow : Ti.App.Properties.getString("openWindow"),
		};
		Alloy.Globals.dashboard.addView(Alloy.createController('signIn', windowData).getView());
		Ti.App.Properties.setString("openWindow", "signIn");
	}
});

function addToCart(cartData) {
	var bind = cartData.bindId;
	var index = cartData.itemIndex;
	var a = cartData.section.items[index];
	productdata = cartData;
	Ti.API.info(' data========================== = ' + JSON.stringify(cartData));
	Ti.API.info('properties = ' + a.properties.foodTruckId);
	Ti.API.info(' _a[_bind].collectionName, = ' + JSON.stringify(a[bind]));
	Ti.App.Properties.setString("truckId", a.properties.foodTruckId);
	var url = commonFile.addCartApi;
	var params = {};
	var params = {
		customer_id : Ti.App.Properties.getString("userId"),
		truck_id : a.properties.foodTruckId,
		item_id : a.properties.productId,
		quantity : a["quantity"].text,
		price : a[cartData.bindId].price,
		order_id : Ti.App.Properties.getString("orderId") || 0,
	};

	commonFile.webServiceCall(url, JSON.stringify(params), addCartCallBack, "POST", $.foodTruckSuperView);

}

function addCartCallBack(response) {
	Ti.API.info('response = ' + JSON.stringify(response));
	if (response.status == true) {

		var bind = productdata.bindId;
		var index = productdata.itemIndex;
		var a = productdata.section.items[index];

		a['quantityContainer'].height = Ti.UI.SIZE;
		a['addLbl'].height = "0dp";
		productdata.section.updateItemAt(productdata.itemIndex, a);

		commonFile.showAlert($.foodTruckSuperView, "Product was successfully added to your cart.");
		Ti.App.Properties.setBool("cartFlag", true);
		$.cartLbl.visible = true;
		Ti.App.Properties.setString("orderId", response.data.id);
	} else {
		commonFile.hideFullLoader($.foodTruckSuperView);
		commonFile.showAlert($.foodTruckSuperView, "Something went wrong");
	}
}

function openCart() {
	var cartData = {
		previousWindow : "foodTruckDetail",
	};
	Alloy.Globals.dashboard.addView(Alloy.createController('yourCart', cartData).getView());
	Ti.App.Properties.setString("openWindow", "yourCart");
}