// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

Alloy.Globals.yourCart = null;
Alloy.Globals.yourCart = $;

var qty = 0;
var subTotal = 0;

/* Add require files */
var commonFile = require('common');

commonFile.createTouchEffect($.checkout, "#a6ffffff", "#ffffff");
/*
 * require header view
 */
var headerView = require('headerView').createHeaderView("yourCart", "YOUR CART", {
	menu : false,
	view : true,
	backgroundimage : false,
}, goToBack);
$.cartSuperView.add(headerView);


$.countLbl.backgroundColor = Ti.App.Properties.getString('appPrimaryColor')||"#CB202D";
$.checkout.backgroundColor = Ti.App.Properties.getString('appPrimaryColor')||"#CB202D";
/*
 * clear memory on back
 */
function goToBack() {
	$.yourCart.animate({
		opacity : 0,
		duration : 200
	}, function() {
		setTimeout(function() {
			Ti.App.Properties.setString("openWindow", args.previousWindow);
			Alloy.Globals.yourCart = null;
			commonFile = null;
			qty = null;
			subTotal = null;
			$.yourCart.removeAllChildren();
			$.yourCart.parent.remove($.yourCart);

		}, 201);
	});
};

exports.goToBack = goToBack;

$.yourCart.animate({
	opacity : 1,
	duration : 200
});

for (var i = 0; i < 20; i++) {
	var mainContainer = Ti.UI.createView({
		top : "8dp",
		left : "8dp",
		width : "240dp",
		height : Ti.UI.SIZE,
		layout : "vertical",
		backgroundColor : "#ffffff"
	});

	// if(i==0){
	// mainContainer.left= "0dp";
	// }else{
	// mainContainer.left= "7dp";
	// }
	var menuImage = Ti.UI.createImageView({
		left : "5dp",
		right : "5dp",
		top : "5dp",
		height : "130dp",
		width : Ti.UI.FILL,
		image : "/images/menu2.jpg",
	});

	if (i == 0) {
		menuImage.image = "/images/browni.png";
	} else if (i == 1) {
		menuImage.image = "/images/browni2.jpg";
	}

	mainContainer.add(menuImage);

	var smallDescription = Ti.UI.createLabel({
		top : "10dp",
		text : "Super saver cmbo (Dessert+Drink)",
		font : {
			fontSize : "12dp",
			fontFamily : "Roboto-Regular"
		},
		left : "5dp",
		right : "5dp",
		width : Ti.UI.FILL,
		color : "#333333"
	});

	mainContainer.add(smallDescription);

	var priceContainer = Ti.UI.createView({
		top : "13dp",
		left : "5dp",
		right : "5dp",
		height : "30dp",
		//borderColor:"green",

	});

	mainContainer.add(priceContainer);

	var priceLbl = Ti.UI.createLabel({
		left : "0dp",
		text : "$78.00",
		font : {
			fontSize : "13dp",
			fontFamily : "Roboto-Regular"
		},
		color : "#333333",
		textAlign : Titanium.UI.TEXT_ALIGNMENT_CENTER,
	});

	priceContainer.add(priceLbl);

	var addLbl = Ti.UI.createLabel({
		text : "+ Add",
		right : "0dp",
		height : "25dp",
		width : "50dp",
		borderRadius : "4dp",
		backgroundColor : Ti.App.Properties.getString('appPrimaryColor')||"#CB202D",
		color : "#ffffff",
		font : {
			fontSize : "12dp",
			fontFamily : "Roboto-Regular"
		},
		textAlign : Titanium.UI.TEXT_ALIGNMENT_CENTER
	});

	priceContainer.add(addLbl);

	var bottomSeperator = Ti.UI.createView({
		top : "8dp",
		height : "8dp",
		width : Ti.UI.FILL,
		backgroundColor : "#e6e6e6",

	});

	mainContainer.add(bottomSeperator);

	$.otherMenu.add(mainContainer);

};

function cartList() {
	commonFile.showFullLoader($.cartSuperView);
	var url = commonFile.cartListApi;
	var params = {};
	var params = {
		customer_id : Ti.App.Properties.getString("userId"),
		//truck_id : Ti.App.Properties.getString("truckId"),
		order_id : ((Ti.App.Properties.getString("orderId") != null)?Ti.App.Properties.getString("orderId"):0)
	};

	commonFile.webServiceCall(url, JSON.stringify(params), cartListCallBack, "POST", $.cartSuperView);
}

cartList();

function cartListCallBack(response) {
	//alert('response = ' + JSON.stringify(response));

	if (response.status == true && response.data.length != 0) {
		$.itemInfoContainer.visible = true;
		$.checkout.touchEnabled = true;
		$.checkout.visible = true;
		
		loadData(response.data);
	} else {
		var listData = [];
		listData.push({
			properties : {

			},
			template : 'emptyTemplate',
		});
		$.cartSection.setItems(listData);
		
		$.itemInfoContainer.visible = false;
		$.checkout.touchEnabled = false;
		$.checkout.visible = false;
		$.totalLbl.setText("");
		commonFile.hideFullLoader($.cartSuperView);
		commonFile.showAlert($.cartSuperView, "Your cart is empty");
	}
}

function loadData(data) {

	var listData2 = [];
	qty = 0;
	subTotal = 0;
	$.cartSection.setItems(listData2);
	_.each(data, function(value, key) {

		qty = parseInt(qty + parseInt(value.quantity));
		subTotal = parseFloat(subTotal + (parseInt(value.quantity)*parseFloat(value.price)));

		listData2.push({
			properties : {
				productId : value.id
			},
			indicationIcon : {
				text : commonFile.circleIcon,
				color : (value.veg == 0 ? "#DA251E" : "#47d147"),
				borderColor : (value.veg == 0 ? "#DA251E" : "#47d147")
			},
			minus : {
				text : commonFile.minusIcon,
				price: value.price,
				qty : value.quantity
			},
			plus : {
				text : commonFile.plusIcon,
				price: value.price,
				qty : value.quantity
			},
			categoryName : {
				color : Ti.App.Properties.getString('appPrimaryColor')||"#CB202D",
				text : value.category_name
			},
			productName : {
				text : value.name
			},
			productDescription : {
				text : value.description
			},
			productPrice : {
				text : "$" + value.price
			},
			productQnt : {
				text : parseInt(value.quantity)
			},
			// quantityContainer:{
			// height:"0dp"
			// },
			// addLbl:{
			// height:"25dp"
			// },
			template : 'cartTemplate',
		});

	}, commonFile.hideFullLoader($.cartSuperView));

	$.cartSection.appendItems(listData2);
	$.countLbl.setText(qty);
	$.totalLbl.setText("Total :  $" + subTotal);

};

$.listView.addEventListener('itemclick', function(e) {
	var bind = e.bindId;
	var index = e.itemIndex;
	var a = e.section.items[index];
	
	if(e.bindId == "plus"){
		a['productQnt'].text = (a['productQnt'].text+1);
		e.section.updateItemAt(e.itemIndex, a);
		updateCart(e);
	}else if(e.bindId == "minus"){
		if(a['productQnt'].text >0){
		a['productQnt'].text = (a['productQnt'].text-1);
		e.section.updateItemAt(e.itemIndex, a);
		updateCart(e);
		}
	}
});


function updateCart(cartData) {
	var bind = cartData.bindId;
	var index = cartData.itemIndex;
	var a = cartData.section.items[index];
	productdata = cartData;
	//Ti.App.Properties.setString("truckId", a.properties.foodTruckId);
	var url = commonFile.addCartApi;
	var params = {};
	var params = {
		customer_id : Ti.App.Properties.getString("userId"),
		truck_id : Ti.App.Properties.getString("truckId"),
		item_id : a.properties.productId,
		quantity : a["productQnt"].text,
		price : a[cartData.bindId].price,
		order_id : ((Ti.App.Properties.getString("orderId") != null)?Ti.App.Properties.getString("orderId"):0)
	};

	commonFile.webServiceCall(url, JSON.stringify(params), updateCartCallBack, "POST", $.cartSuperView);

}

function updateCartCallBack(response) {
	Ti.API.info('response = ' + JSON.stringify(response));
	if (response.status == true) {

		var bind = productdata.bindId;
		var index = productdata.itemIndex;
		var a = productdata.section.items[index];

		// a['quantityContainer'].height = Ti.UI.SIZE;
		// a['addLbl'].height = "0dp";
		// productdata.section.updateItemAt(productdata.itemIndex, a);

		commonFile.showAlert($.cartSuperView, "Product was successfully updated.");
		Ti.App.Properties.setString("orderId", response.data.id);
		cartList();
	} else {
		commonFile.hideFullLoader($.cartSuperView);
		commonFile.showAlert($.cartSuperView, "Something went wrong");
	}
}

$.checkout.addEventListener('click', function(e) {
	var windowData = {
		previousWindow : "yourCart",
	};
	Alloy.Globals.dashboard.addView(Alloy.createController('orderOption', windowData).getView());
	Ti.App.Properties.setString("openWindow", "orderOption");

	//Ti.API.info('qty = ' + qty);
	//alert(qty + " === " + subTotal);

});
