// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

Alloy.Globals.orderOption = null;
Alloy.Globals.orderOption = $;

/* Add require files */
var commonFile = require('common');

commonFile.createTouchEffect($.submitOrderTime,"#a6ffffff","#ffffff");

$.rightNow.setText(commonFile.circleIcon);
$.in30min.setText(commonFile.circleIcon);
$.in45min.setText(commonFile.circleIcon);


$.submitOrderTime.backgroundColor = Ti.App.Properties.getString('appPrimaryColor')||"#CB202D";
/*
 * require header view
 */
var headerView = require('headerView').createHeaderView("orderOption", "CHOOSE OPTION", {
	menu : false,
	view : true,
	backgroundimage : false
}, goToBack);
$.orderOptionSuperView.add(headerView);



var data = [$.notes];
$.orderOption.addEventListener('click', function(e) {
	commonFile.blurTextfield(e, data);
});


/*
 * clear memory on back
 */
function goToBack() {
	$.orderOption.animate({
		opacity : 0,
		duration : 200
	}, function() {
		setTimeout(function() {
			Ti.App.Properties.setString("openWindow", args.previousWindow);
			Alloy.Globals.orderOption = null;
			commonFile = null;
			$.orderOption.removeAllChildren();
			$.orderOption.parent.remove($.orderOption);

		}, 201);
	});
};

exports.goToBack = goToBack;

$.orderOption.animate({
	opacity : 1,
	duration : 200
});

var selectOption = null;

function chooseOption(e) {
	// Ti.API.info('e.id '+ e.id);
	// Ti.API.info('e.id '+ JSON.stringify(e.source.children));

	if (e.source.children[0].id == "rightNow") {
		$.rightNow.setColor(commonFile.headerColor);
		$.in30min.setColor("#33000000");
		$.in45min.setColor("#33000000");
		selectOption = 0;
	} else if (e.source.children[0].id == "in30min") {

		$.rightNow.setColor("#33000000");
		$.in30min.setColor(commonFile.headerColor);
		$.in45min.setColor("#33000000");
		selectOption = 30;

	} else if (e.source.children[0].id == "in45min") {
		$.rightNow.setColor("#33000000");
		$.in30min.setColor("#33000000");
		$.in45min.setColor(commonFile.headerColor);
		selectOption = 45;
	}

}

function navigateToNext(e) {

	if (selectOption != null) {
		//Alloy.Globals.navigateToNext("orderSummary");
	} else {
		alert("Please Choose Pick-up Option");
	}

}



function submitOrderSummary() {
	if (selectOption != null) {
		commonFile.showFullLoader($.orderOptionSuperView);
		var url = commonFile.placeOrderApi;
		var params = {};
		var params = {
			customer_id : Ti.App.Properties.getString("userId"),
			truck_id : Ti.App.Properties.getString("truckId"),
			order_id : Ti.App.Properties.getString("orderId"),
			pickup_option : selectOption,
			note : $.notes.getValue(),
		};
	
		commonFile.webServiceCall(url, JSON.stringify(params), orderCallBack, "POST", $.orderOptionSuperView);
	}else {
		commonFile.showAlert($.orderOptionSuperView, "Please Choose Pick-up Option");
	}
}

function orderCallBack(response) {
	Ti.API.info('response = ' + JSON.stringify(response));

	if (response.status == true) {
		commonFile.hideFullLoader($.orderOptionSuperView);
		commonFile.showAlert($.orderOptionSuperView, "Order placed successfully");
		 Ti.App.Properties.setString("truckId",null);
		 Ti.App.Properties.setString("orderId",0);
		 Alloy.Globals.foodTruckDetail.goToBack();
		 Alloy.Globals.yourCart.goToBack();
		 
		 var thanksView = Ti.UI.createView({
		 	width:Ti.UI.FILL,
		 	height:Ti.UI.FILL,
		 	backgroundColor:"#ffffff",
		 	backgroundImage:"/images/"
		 });
		 
		 var imgView = Ti.UI.createImageView({
		 	width:Ti.UI.FILL,
		 	height:Ti.UI.FILL,
		 	top:"0dp",
		 	left:"0dp",
		 	backgroundImage:"/images/bg.png"
		 });
		 
		 thanksView.add(imgView);
		 
		 var lbl = Ti.UI.createLabel({
		 	text:"YOUR ORDER HAS BEEN PLACED \n THANK YOU !!!",
		 	textAlign:Titanium.UI.TEXT_ALIGNMENT_CENTER,
		 	width:"85%",
		 	font:{
		 		fontSize:"25",
		 		fontFamily:"roboto.bold"
		 	},
		 	color:Ti.App.Properties.getString('appPrimaryColor')||"#CB202D",
		 	//top:"150dp",
		 });
		 
		 var btn = Ti.UI.createLabel({
		 	text:"Ok",
		 	width:"85%",
		 	textAlign:Titanium.UI.TEXT_ALIGNMENT_CENTER,
		 	height:"40dp",
		 	font:{
		 		fontSize:"15",
		 		fontFamily:"Roboto-Regular"
		 	},
		 	
		 	backgroundColor:Ti.App.Properties.getString('appPrimaryColor')||"#CB202D",
		 	color:"#ffffff",
		 	bottom:"40dp",
		 	borderRadius:"3dp"
		 });
		 
		 thanksView.add(lbl);
		 thanksView.add(btn);
		 
		 commonFile.createTouchEffect(btn, "#a6ffffff", "#ffffff");
		 
		 btn.addEventListener('click',function(){
		 	goToBack();
		 });
		 $.orderOption.add(thanksView);
		 // setTimeout(function(){
		 // //	 goToBack();
		 // },2000);
		
	} else {
		commonFile.hideFullLoader($.orderOptionSuperView);
		commonFile.showAlert($.orderOptionSuperView, "Something went wrong");
	}
}

$.notes.addEventListener('change',function(e){
	var remaining = (40-$.notes.getValue().length);
	$.charLength.setText(remaining+" characters left");	
});
