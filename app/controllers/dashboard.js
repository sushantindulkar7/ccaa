// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var commonFile = require('common');

Alloy.Globals.dashboard = $;

Alloy.Globals.dashboardId = $.dashboard;

/*
 * initialize navigation drawer
 */
var drawer = require('navdrawer').createDrawer("truckLocator", args, "leftSlider");
$.dashboard.add(drawer);

Ti.App.Properties.setString("openWindow","truckLocator");

///$.dashboard.add(Alloy.createController('reachOutToSchool').getView());
exports.addView = function (view) { 
	$.dashboard.add(view);
};

//exports.addView = addView;

function closeWindow(){
	Ti.API.info('openWindow = ' + Ti.App.Properties.getString("openWindow"));
	try {
		if (!commonFile.isNullVal(Ti.App.Properties.getString('openWindow'))) {
			if (Ti.App.Properties.getString("openWindow") == "popups") {
				require('popups').closePopup();
			} else if (Ti.App.Properties.getString("openWindow") == "foodTruckDetail") {
				Alloy.Globals.foodTruckDetail.goToBack();
			} else if (Ti.App.Properties.getString("openWindow") == "yourCart") {
				Alloy.Globals.yourCart.goToBack();
			} else if (Ti.App.Properties.getString("openWindow") == "truckLocator") {
				args = null;
				$.dashboard.close();
				$.destroy();
			} else if (Ti.App.Properties.getString("openWindow") == "orderOption") {
				Alloy.Globals.orderOption.goToBack();
			} else if (Ti.App.Properties.getString("openWindow") == "orderSummary") {
				Alloy.Globals.orderSummary.goToBack();
			} else if (Ti.App.Properties.getString("openWindow") == "signIn") {
				Alloy.Globals.signIn.goToBack();
			}else if (Ti.App.Properties.getString("openWindow") == "verification") {
				Alloy.Globals.verification.goToBack();
			}else if (Ti.App.Properties.getString("openWindow") == "pastOrder") {
				Alloy.Globals.pastOrder.goToBack();
			}else if (Ti.App.Properties.getString("openWindow") == "contactUs") {
				Alloy.Globals.contactUs.goToBack();
			}else if (Ti.App.Properties.getString("openWindow") == "rateUs") {
				Alloy.Globals.rateUs.goToBack();
			}else if (Ti.App.Properties.getString("openWindow") == "terms_condition") {
				Alloy.Globals.terms_condition.goToBack();
			}
			
		} else {
			Ti.API.info('reachOutToSchool 2 ');
			args = null;
			$.dashboard.close();
			$.destroy();
		}
	} catch(ex) {
		Ti.API.info('catch error = ' + ex.message);
	}
};
exports.closeWindow = closeWindow;
exports.gotoBack = function gotoBack() {
	Ti.API.info('goto back call');
	args = null;
	$.dashboard.close();
	$.destroy();
};
// setTimeout(function(){
// showAlert($.dashboard, "Welcome "+args);
// },3000);
