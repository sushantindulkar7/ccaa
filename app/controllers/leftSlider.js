// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

Alloy.Globals.leftSlider = null;
Alloy.Globals.leftSlider = $;

var commonFile = require('common');

$.l1.text = commonFile.homeIcon;
$.l2.text = commonFile.clockIcon;
//$.l3.text = commonFile.bellIcon;
$.l4.text = commonFile.phoneIcon;
$.l5.text = commonFile.starIcon;
$.l6.text = commonFile.noteIcon;

$.emailLabel.color = Ti.App.Properties.getString('appPrimaryColor')||"#CB202D";
$.leftSlider.backgroundColor = Ti.App.Properties.getString('sliderColor')||"#333333",

$.leftSliderScw.addEventListener('click', function(e) {

	//commonFile.createTouchEffect(e.source.children[0],"#CB202D","#a6a6a6");
	//commonFile.createTouchEffect(e.source.children[1],"#CB202D","#a6a6a6");
	//Ti.API.info('e=='+JSON.stringify(e));
	var drawer = require('navdrawer').toggleEffect();
	drawer = null;

	Ti.API.info('Ti.App.Properties.getString("openWindow") ==== ' + Ti.App.Properties.getString("openWindow"));

	switch(e.source.value) {
	case "home":
		//'Alloy.Globals.'+Ti.App.Properties.getString("openWindow")+'.goToBack()';
		if (Ti.App.Properties.getString("openWindow") != "truckLocator") {

			//require('navdrawer').removeWindow(Alloy.Globals.pastOrderId);

			Alloy.Globals.dashboard.closeWindow();
			//Ti.App.Properties.setString("openWindow","truckLocator");
		}
		Ti.App.Properties.setString("openWindow", "truckLocator");
		//Alloy.Globals.dashboard.addView(Alloy.createController('addNewListing').getView());
		//Ti.App.Properties.setString("openWindow", "addNewListing");
		break;
	case "pastOrder":
		//Alloy.Globals.Ti.App.Properties.getString("openWindow").goToBack();
		if (Ti.App.Properties.getString("openWindow") != "pastOrder") {

			if (Ti.App.Properties.getString("userId") != null || Ti.App.Properties.getString("userId") != undefined) {
				var windowData = {
					previousWindow : Ti.App.Properties.getString("openWindow"),
				};
				Alloy.Globals.dashboard.addView(Alloy.createController('pastOrder', windowData).getView());
				Ti.App.Properties.setString("openWindow", "pastOrder");
			} else {
				var windowData = {
					previousWindow : Ti.App.Properties.getString("openWindow"),
				};
				Alloy.Globals.dashboard.addView(Alloy.createController('signIn', windowData).getView());
				Ti.App.Properties.setString("openWindow", "signIn");
			}

		}
		break;

	case "contactUs":

		if (Ti.App.Properties.getString("openWindow") != "contactUs") {
			var windowData = {
				previousWindow : Ti.App.Properties.getString("openWindow"),
			};

			//require('navdrawer').setCenterView(Alloy.createController('contactUs', windowData).getView());

			Alloy.Globals.dashboard.addView(Alloy.createController('contactUs', windowData).getView());

			//var drawer = require('navdrawer').setCenterView("pastOrder");

			//Alloy.Globals.dashboard.addView(Alloy.createController('pastOrder', windowData).getView());
			Ti.App.Properties.setString("openWindow", "contactUs");

		}
		break;

	case "notification":
		var windowData = {
			previousWindow : Ti.App.Properties.getString("openWindow"),
		};
		Alloy.Globals.dashboard.addView(Alloy.createController('signIn', windowData).getView());
		Ti.App.Properties.setString("openWindow", "signIn");
		break;

	case "rateUs":
		Alloy.Globals.dashboard.addView(Alloy.createController('rateUs', windowData).getView());
		Ti.App.Properties.setString("openWindow", "rateUs");

		break;
	case "terms":

		if (Ti.App.Properties.getString("openWindow") != "terms_condition") {
			var windowData = {
				previousWindow : Ti.App.Properties.getString("openWindow"),
			};

			Alloy.Globals.dashboard.addView(Alloy.createController('terms_condition', windowData).getView());

			Ti.App.Properties.setString("openWindow", "terms_condition");

		}
		break;
	case "login":

		Ti.API.info('e.source.text = ' + e.source.text);
		if ((e.source.text == "WELCOME GUEST !" || e.source.text == "Login/Register")) {
			var windowData = {
				previousWindow : Ti.App.Properties.getString("openWindow"),
			};
			Alloy.Globals.dashboard.addView(Alloy.createController('signIn', windowData).getView());
			Ti.App.Properties.setString("openWindow", "signIn");
		}
		break;

	}

});

function setUserData() {

	$.nameLabel.setText("WELCOME " + Ti.App.Properties.getString("userName"));
	$.emailLabel.setText(Ti.App.Properties.getString("userPhone"));
}

exports.setUserData = setUserData;

if (!commonFile.isNullVal(Ti.App.Properties.getString('userName'))) {
	$.nameLabel.setText("WELCOME " + Ti.App.Properties.getString("userName"));
	$.emailLabel.setText(Ti.App.Properties.getString("userPhone"));
}
// $.signOut.addEventListener('click', function(e) {
// Ti.App.Properties.setString('accountId',"");
// Alloy.createController('signIn').getView().open();
//
//
//
// Alloy.Globals.googleAuth.deAuthorize(function(e) {
// //Ti.API.info('google logout');
// }, Alloy.Globals.dashboardId);
//
// Alloy.Globals.dashboard.gotoBack();
//
// });