// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

Alloy.Globals.contactUs = null;
Alloy.Globals.contactUs = $;

/* Add require files */
var commonFile = require('common');

/*
 * require header view
 */
var headerView = require('headerView').createHeaderView("contactUs", "CONTACT US", {
	menu : false,
	view : true,
	backgroundimage : false
}, goToBack);
$.contactUsSuperView.add(headerView);

/*
 * clear memory on back
 */
function goToBack() {
	
	$.contactUs.animate({
		opacity : 0,
		duration : 200
	}, function() {
		setTimeout(function() {
			Ti.App.Properties.setString("openWindow",args.previousWindow);
			Alloy.Globals.contactUs = null;
			commonFile = null;
			$.contactUs.removeAllChildren();
			$.contactUs.parent.remove($.contactUs);
			//require('navdrawer').removeWindow($.contactUs);
			
		}, 201);
	});
	
	
	
};

exports.goToBack = goToBack;

$.contactUs.animate({
	opacity : 1,
	duration : 200
});
