// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

Alloy.Globals.orderSummary = null;
Alloy.Globals.orderSummary = $;

/* Add require files */
var commonFile = require('common');
var qty = 0;
var	subTotal = 0;
var orderTime = null;

/*
 * require header view
 */
var headerView = require('headerView').createHeaderView("orderSummary", "ORDER SUMMARY", {
	menu : false,
	view : true,
	backgroundimage : false
}, goToBack);
$.orderSummarySuperView.add(headerView);


/*
 * clear memory on back
 */
function goToBack() {
	$.orderSummary.animate({
		opacity : 0,
		duration : 200
	}, function() {
		setTimeout(function() {
			Ti.App.Properties.setString("openWindow", args.previousWindow);
			Alloy.Globals.orderSummary = null;
			commonFile = null;
			$.orderSummary.removeAllChildren();
			$.orderSummary.parent.remove($.orderSummary);

		}, 201);
	});
};

exports.goToBack = goToBack;

$.orderSummary.animate({
	opacity : 1,
	duration : 200
});




function orderList() {
	commonFile.showFullLoader($.orderSummarySuperView);
	
	var params = {};
	
	var truckId = null;
	var orderId = null;
	
	if(args.from == "pastOrder"){
		truckId = "";
		orderId = args.order_id;
		
		var url = commonFile.orderDetail+orderId;
		var params = {};
		commonFile.webServiceCall(url, JSON.stringify(params), orderListCallBack, "GET", $.orderSummarySuperView);
	}else{
		truckId = Ti.App.Properties.getString("truckId");
		//orderId = Ti.App.Properties.getString("orderId");
		
		var url = commonFile.cartListApi;
		var params = {
			customer_id : Ti.App.Properties.getString("userId"),
			//truck_id : truckId,
			order_id : ((Ti.App.Properties.getString("orderId") != null)?Ti.App.Properties.getString("orderId"):0),
		};
		
		commonFile.webServiceCall(url, JSON.stringify(params), cartListCallBack, "POST", $.orderSummarySuperView);
	
	}
	
	

	
}

orderList();	



function orderListCallBack(response) {
	Ti.API.info('response = ' + JSON.stringify(response));

	if (response.status == true) {
		orderTime = response.data.order[0].order_placed_time;
		loadData(response.data.items);
	} else {
		commonFile.hideFullLoader($.orderSummarySuperView);
		commonFile.showAlert($.orderSummarySuperView, "Something went wrong");
	}
}


function cartListCallBack(response) {
	Ti.API.info('response = ' + JSON.stringify(response));

	if (response.status == true) {
		orderTime = new Date();
		loadData(response.data);
	} else {
		commonFile.hideFullLoader($.orderSummarySuperView);
		commonFile.showAlert($.orderSummarySuperView, "Something went wrong");
	}
}



function loadData(data) {

	var listData2 = [];
	qty = 0;
	subTotal = 0;
	//$.cartSection.setItems(listData2);
	_.each(data, function(value, key) {

		qty = parseInt(qty + parseInt(value.quantity));
		subTotal = parseFloat(subTotal + (parseInt(value.quantity)*parseFloat(value.price)));
		//orderTime = value.order_placed_time;
  
  var mainContainer = Ti.UI.createView({
  	width:Ti.UI.FILL,
  	height:"30dp",
  	//borderColor:"green",
  	
  });
  
  var quantity = Ti.UI.createLabel({
  	width:"20dp",
  	left:"0dp",
  	color:"#333333",
  	height:Ti.UI.FILL,
  	font:{
  		fontSize:"14dp",
  		fontFamily:"roboto_light"
  	},
  	//borderColor:"yellow",
  	text:value.quantity
  });
  
  mainContainer.add(quantity);
  
  
  var crossLbl = Ti.UI.createLabel({
  	width:"15dp",
  	height:Ti.UI.FILL,
  	left:"25dp",
  	color:"#333333",
  	font:{
  		fontSize:"14dp",
  		fontFamily:"icomoon"
  	},
  	text:"x",
  	textAlign:Titanium.UI.TEXT_ALIGNMENT_CENTER,
  	//borderColor:"blue"
  });
  
  mainContainer.add(crossLbl);
   
  var name = Ti.UI.createLabel({
  	width:Ti.UI.FILL,
  	left:"50dp",
  	right:"80dp",
  	color:"#333333",
  	height:Ti.UI.FILL,
  	wordWrap:false,
  	ellipsize:true,
  	font:{
  		fontSize:"14dp",
  		fontFamily:"roboto_light"
  	},
  	text:value.name || "Product",
  //	borderColor:"blue"
  });
  
  mainContainer.add(name);
  
  var price = Ti.UI.createLabel({
  	//width:"150dp",
  	right:"0dp",
  	color:"#333333",
  	height:Ti.UI.FILL,
  	font:{
  		fontSize:"14dp",
  		fontFamily:"roboto_light"
  	},
  	text:"$"+(value.price*value.quantity),
  });
  
  mainContainer.add(price);
  
  $.itemScroll.add(mainContainer);
  
  },setOtherData());
  
  $.totalItems.setText(qty+" Items");
	$.totalPriceLbl.setText("Sub Total    $"+subTotal);
	$.grandTotalLbl.setText("$"+subTotal);
	
	
    var d = new Date(orderTime);
    var n = d.toUTCString();
    var finalDate = n.slice(0,16);
    
    
    
    /*
 * set attributestring to store locator label
 */
var text = "\ue930 "+finalDate;
var attr = Ti.UI.createAttributedString({
    text : text,
    attributes : [{
        type : Ti.UI.ATTRIBUTE_FONT,
        value : {
            fontSize:"18dp",
            fontFamily:"roboto_medium"
        },
        range : [text.indexOf(finalDate), (finalDate).length]
    }]
});

$.orderDate.attributedString = attr;

   // $.orderDate.setText(finalDate);
    
    
}


function setOtherData(){
	commonFile.hideFullLoader($.orderSummarySuperView);
	
}



