// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

/* Add require files */
var commonFile = require('common');
var coords = null;
var callApiFlag = true;
var longs_,lat_ = null;
$.truckLocatorSuperView.top = commonFile.headerHeight;

var openPageFlag = true;
var eventFlag = false;
var userCurrentLocation = null;
//$.v1.transform= Ti.UI.create2DMatrix().rotate(90);

/*
 * require header view
 */
var headerView = require('headerView').createHeaderView("truckLocator", "CHILLI CROSS", {
	menu : true,
	view : true,
	rightMenu : true,
	backgroundimage : false
});
$.truckLocator.add(headerView);

var mapview,
    map = "";
map = require('ti.map');

/* Landing Image Effect*/
// setTimeout(function(e) {
// $.landingImage.animate({
// opacity : 0,
// duration : 700
// }, function(e) {
// $.truckLocator.remove($.landingImage);
// setTimeout(function(e) {
// geolocation();
// }, 1500);
// });
// }, 2000);

setTimeout(function(e) {
	geolocation();
}, 1500);

// setTimeout(function(e) {
// $.landingImage.animate({
// opacity : 0,
// duration : 700
// }, function(e) {
// $.truckLocator.remove($.landingImage);
// setTimeout(function(e) {
// geolocation();
// }, 1500);
// });
// }, 2000);

function showAnnotation(coordsData) {

	Ti.API.info('coordsData = ' + JSON.stringify(coordsData));
	//alert(JSON.stringify(coordsData));

	commonFile.showFullLoader($.truckLocatorSuperView);
	var url = commonFile.gpsApi;
	var params = {};
	lat_ = coordsData.latitude;
	longs_ = coordsData.longitude;
	var params = {
		latitude : parseFloat(lat_.toFixed(4)),
		longitude : parseFloat(longs_.toFixed(4)),
		radious : 100,
		logo_url : "qwerqwe/",
		customer_id : ((Ti.App.Properties.getString("userId") != null)?Ti.App.Properties.getString("userId"):0)
	};

	commonFile.webServiceCall(url, JSON.stringify(params), gpsCallBack, "POST", $.truckLocator);
}

function gpsCallBack(response) {

	eventFlag = false;
	//alert('response = ' + JSON.stringify(response));
	if (response.status == true) {

		//if (parseInt(Titanium.Platform.version) < 5) {

		if (response.data.trucks.length != 0) {
//alert("if 1");
			coords = {
				latitude : lat_,
				longitude : longs_,
			};
			//setTimeout(function(e) {
			loadMap(response.data.trucks);
			//	}, 3500);

			setTimeout(function() {
				//mapview.addEventListener('regionchanged', setMapdata);
			}, 5000);

			//callApiFlag = false;
		}else{
			checkMap();
		commonFile.hideFullLoader($.truckLocatorSuperView);
		commonFile.showAlert($.truckLocatorSuperView, "No trucks available nearby, search other location.");
		}
		//	} else {
		//	Ti.API.info('inside else');
		//	}
	} else {
		
		//alert("inside else");
		setTimeout(function() {
			//mapview.addEventListener('regionchanged', setMapdata);
		}, 5000);

		// if (openPageFlag == true) {
		// $.landingImage.animate({
		// opacity : 0,
		// duration : 700
		// }, function(e) {
		// $.truckLocator.remove($.landingImage);
		// $.truckLocator.remove($.locationLbl);
		//
		// });
		//
		// $.locationLbl.animate({
		// opacity : 0,
		// duration : 700
		// }, function(e) {
		// $.truckLocator.remove($.locationLbl);
		//
		// });
		//
		// $.truckLocatorSuperView.animate({
		// opacity : 1,
		// duration : 700
		// });
		//
		// fetchAddress(coords.latitude, coords.longitude);
		// openPageFlag = false;
		// }
		//hideView();
		
		// var now = new Date().getTime();
		// var remindToRate = Ti.App.Properties.getString('RemindToRate');
		// if (!remindToRate) {
			// Ti.App.Properties.setString('RemindToRate', now);
			// hideView();
		// } else if (remindToRate < now) {
			// checkTheme();
		// } else {
		//	hideView();
		//}
	
		//if(Ti.App.Properties.getBool('appActive') == true){
			checkMap();
		// }else{
			// hideView();
		// }

		commonFile.hideFullLoader($.truckLocatorSuperView);
		commonFile.showAlert($.truckLocatorSuperView, "No trucks available nearby, search other location.");
	}
}

// if (commonFile.isNullVal(Ti.App.Properties.getString("firstTime"))) {
	// setTimeout(function(e) {
		// var windowData = {
			// previousWindow : "truckLocator",
		// };
		// Alloy.Globals.dashboard.addView(Alloy.createController('signIn', windowData).getView());
		// Ti.App.Properties.setString("openWindow", "signIn");
	// }, 1000);
	// Ti.App.Properties.setString("firstTime", true);
// }

// mapview.addEventListener('regionchanged', function(evt) {
// Ti.API.info('regionchanged = '+JSON.stringify(evt));
// });
 
function loadMap(gpsData) {

	try {
//alert("if 2");
		// var mapview = map.createView({
		// mapType : map.NORMAL_TYPE,
		// region : {
		// latitude : coords.latitude,
		// longitude : coords.longitude,
		// latitudeDelta : 0.05,
		// longitudeDelta : 0.05
		// },
		// animate : true,
		// regionFit : true,
		// userLocation : true,
		// annotations : createAnnotations(),
		// //annotations : [mountainView, mountainView1, mountainView2]
		// });
		// mapview.region = {
		// latitude : coords.latitude,
		// longitude : coords.longitude,
		// latitudeDelta : 0.01,
		// longitudeDelta : 0.01
		// };
		//mapview.annotations = createAnnotations();

		//	function createAnnotations() {
		var annoatationData = [];
		var mapAnnotation = [];
		//alert(anotationImage);
		//for (var i = 0; i < 10; i++) {
		_.each(gpsData, function(value, key) {

			// var abc = Ti.UI.createLabel({
			// width : 50,
			// height : 70,
			// backgroundColor : "transparent",
			// backgroundImage : "/images/anotation.png",
			// //borderColor:"green"
			// });
//alert(value.latitude +" - "+ value.latitude);
			mapAnnotation[key] = map.createAnnotation({
				latitude : value.latitude, //coord.latitude,
				longitude : value.longitude, //coord.longitude,
				title : value.name || "Food Truck",
				subtitle : value.address || "Virar",
				truckImage : value.logo,
				truckDescription : value.description,
				truckName : value.name,
				truckAddress : value.address,
			//	customView : abc, 
				leftButton : "/images/mnavigation.png",
				rightButton : "/images/info.png",
				image : "/images/anotation.png", 
				//image : anotationImage || "/images/anotation.png",
				truckid : value.id, // Custom property to uniquely identify this annotation.
				phone : value.phone
			});

			annoatationData.push(mapAnnotation[key]);
		}, commonFile.hideFullLoader($.truckLocatorSuperView));

//alert("userCurrentLocation.latitude = "+userCurrentLocation.latitude);
		var mountainView = map.createAnnotation({
			latitude : userCurrentLocation.latitude,
			longitude : userCurrentLocation.longitude,
			//pincolor : map.ANNOTATION_RED,
			image : "/images/pin3.png",
			myid : 1 // Custom property to uniquely identify this annotation.
		});

		//	$.mapview.annotations = [mountainView];
		annoatationData.push(mountainView);
		$.mapview.annotations = annoatationData;
		
		//	}
		// if (openPageFlag == true) {
		// $.landingImage.animate({
		// opacity : 0,
		// duration : 700
		// }, function(e) {
		// $.truckLocator.remove($.landingImage);
		// $.truckLocator.remove($.locationLbl);
		//
		// });
		//
		// $.locationLbl.animate({
		// opacity : 0,
		// duration : 700
		// }, function(e) {
		// $.truckLocator.remove($.locationLbl);
		//
		// });
		//
		// $.truckLocatorSuperView.animate({
		// opacity : 1,
		// duration : 700
		// });
		//
		// fetchAddress(coords.latitude, coords.longitude);
		// openPageFlag = false;
		// }

		// var now = new Date().getTime();
		// var remindToRate = Ti.App.Properties.getString('RemindToRate');
		// if (!remindToRate) {
			// Ti.App.Properties.setString('RemindToRate', now);
			// hideView();
		// } else if (remindToRate < now) {
			// checkTheme();
		// } else {
			//hideView();
		//}
		
		//if(Ti.App.Properties.getBool('appActive') == true){
			checkMap();
		// }else{
			// hideView();
		// }

		// var circle = map.createCircle({
			// center : {
				// latitude : coords.latitude,
				// longitude : coords.longitude
			// },
			// radius : 200000, //1km = 1000
			// strokeWidth : 0,
			// fillColor : "transparent"
			// //fillColor : "#ffe6e6"
		// });
		// $.mapview.addCircle(circle);

		// if ($.truckLocatorSuperView.children.length != 0) {
		// $.truckLocatorSuperView.removeAllChildren();
		// $.truckLocatorSuperView.add(mapview);
		// }else{
		// $.truckLocatorSuperView.add(mapview);
		// }

		// Handle click events on any annotations on this map.

		// mapview.addEventListener('click', function(evt) {
		//
		// if (evt.clicksource == "leftPane") {
		// Ti.Platform.openURL("https://www.google.com/maps?saddr=My+Location&daddr=" + evt.latitude + "," + evt.longitude);
		// //https://www.google.com/maps?saddr=My+Location&daddr=43.12345,-76.12345
		// } else if (evt.clicksource == "rightPane") {
		//
		// var truckData = {
		// previousWindow : "truckLocator",
		// truckDescription : evt.annotation.truckDescription,
		// truckName : evt.title,
		// truckAddress : evt.subtitle,
		// truckImage : evt.annotation.truckImage,
		// truckId : evt.annotation.truckid,
		// };
		// Alloy.Globals.dashboard.addView(Alloy.createController('foodTruckDetail', truckData).getView());
		// Ti.App.Properties.setString("openWindow", "foodTruckDetail");
		// } else if (evt.clicksource == "circle") {
		//
		// circle.center = {
		// latitude : evt.latitude,
		// longitude : evt.longitude
		// };
		//
		// var newCoordsData = {
		// latitude : evt.latitude,
		// longitude : evt.longitude
		// };
		// showAnnotation(newCoordsData);
		// }
		// });
		//
		// mapview.addEventListener('regionchanged', function(evt) {
		// //Ti.API.info('regionchanged = '+JSON.stringify(evt));
		//
		// //setTimeout(function(){
		// //alert(evt.latitude+'='+evt.longitude);
		// if(callApiFlag == true){
		// mapview.region = {
		// latitude : evt.latitude,
		// longitude : evt.longitude,
		// latitudeDelta : 0.05,
		// longitudeDelta : 0.05
		// };
		//
		// var mountainView = map.createAnnotation({
		// latitude : evt.latitude,
		// longitude : evt.longitude,
		// //	title : "Appcelerator Headquarters",
		// subtitle : 'Mountain View, CA',
		// pincolor : map.ANNOTATION_RED,
		// draggable : true,
		// myid : 1 // Custom property to uniquely identify this annotation.
		// });
		//
		// //mapview.annotations = [mountainView];
		//
		// circle.center = {
		// latitude : evt.latitude,
		// longitude : evt.longitude
		// };
		//
		// var newCoordsData1 = {
		// latitude : evt.latitude,
		// longitude : evt.longitude
		// };
		//
		// //alert(_.size(newCoordsData1));
		//
		// if(_.size(newCoordsData1) >0){
		// showAnnotation(newCoordsData1);
		// }
		//
		// }else{
		// //alert("else");
		// setTimeout(function(){
		// callApiFlag= true;
		// },10000);
		// }
		// //showAnnotation(newCoordsData1);
		//
		// //},10000);
		// });
		callApiFlag = false;

	} catch(ex) {
		//alert("if 3 catch"+ex.message);
		Ti.API.info('catch = ' + ex.message);
	}

}

function hideView() {
	//alert("if 5");
	if (openPageFlag == true) {
	//	alert("if 6");
		$.landingImage.animate({
			opacity : 0,
			duration : 700
		}, function(e) {
			$.truckLocator.remove($.landingImage);
			$.truckLocator.remove($.locationLbl);

		});

		$.locationLbl.animate({
			opacity : 0,
			duration : 700
		}, function(e) {
			$.truckLocator.remove($.locationLbl);

		});

		$.truckLocatorSuperView.animate({
			opacity : 1,
			duration : 700
		});

		fetchAddress(coords.latitude, coords.longitude);
		openPageFlag = false;
	}
}

function checkTheme() {

	var url = "https://raw.githubusercontent.com/sushantsi/foodtruck/master/foodtruck.json";
	var client = Ti.Network.createHTTPClient({
		// function called when the response data is available
		onload : function(e) {
			//Ti.API.info("Received text: " + this.responseText);
			//alert('success');
			var themeData = JSON.parse(this.responseText);
			try {
				Ti.API.info('data==========================' + JSON.stringify(themeData));
				if (!commonFile.isNullVal(themeData)) {
					if (themeData.status == true) {
						setTimeout(function() {
							checkMap();
						}, 1000);
					}else if(themeData.themeFlag == true){
						Ti.App.Properties.setString('appPrimaryColor',themeData.appPrimaryColor);
						Ti.App.Properties.setString('appSecondaryColor',themeData.appSecondaryColor);
						Ti.App.Properties.setString('sliderColor',themeData.sliderColor);
						//Ti.App.Properties.setString('anotation',themeData.anotation);
						//Ti.App.Properties.setString('mapPin',themeData.mapPin);
						setTimeout(function() {
							hideView();
						}, 1500);
					}else{
						hideView();
					}

				} else {
					hideView();
				}
			} catch(ex) {
				hideView();
			}

		},
		// function called when an error occurs, including a timeout
		onerror : function(e) {
			Ti.API.debug(e.error);
			hideView();
			//alert('error');
		},
		timeout : 5000 // in milliseconds
	});
	// Prepare the connection.
	client.open("GET", url);
	// Send the request.
	client.send();
}

function checkMap() {
	
	hideView();
	// var now = new Date().getTime();
	// var remindToRate = Ti.App.Properties.getString('RemindToRate');
	// if (!remindToRate) {
	// Ti.App.Properties.setString('RemindToRate', now);
	// }
	// else if (remindToRate < now) {
	// var alertDialog = Titanium.UI.createAlertDialog({
	// title: 'New version available',
	// message: 'Please,update app to new version to continue shopping',
	// buttonNames: ['UPDATE', 'REMIND ME LATER', 'NEVER'],
	// cancel: 2
	// });
	//
	// alertDialog.show();
	//
	// //Ti.App.Properties.setString('RemindToRate', now + (1000 * 60 * 60 * 24));
	// //Ti.App.Properties.setString('RemindToRate', Number.MAX_VALUE);
	// }
	//var blob = $.truckLocatorSuperView.toImage();
	// $.truckLocatorSuperView.animate({
	// opacity : 0,
	// duration : 500
	// });
	
	//temp
	// $.truckLocator.add($.landingImage);
	// $.landingImage.opacity = 1;
	// $.landingImage.setImage("http://www.cityrider.com/fixed/43aspect.png");
	//  alert($.truckLocatorSuperView.toImage());
}

$.mapview.addEventListener('click', function(evt) {

	eventFlag = true;

	setTimeout(function() {
		eventFlag = false;
	}, 1400);

	// if (evt.clicksource == "pin") {
	//
	// if ($.anotationView.getVisible() == false) {
	// $.anotationView.visible = true;
	// $.truckName.text = evt.annotation.truckName;
	// $.truckAddress.text = evt.annotation.truckAddress;
	// } else {
	// $.anotationView.visible = false;
	// }
	//
	// }else{
	// $.anotationView.visible = false;
	// }
	Ti.API.info('evt.clicksource = ========================'+evt.clicksource);

	if (evt.clicksource == "leftPane") {
		//Ti.Platform.openURL("https://www.google.com/maps?saddr=My+Location&daddr=" + evt.latitude + "," + evt.longitude);
		Ti.Platform.openURL("https://www.google.com/maps/dir/?api=1&destination=" + evt.latitude + "," + evt.longitude);

		//https://www.google.com/maps?saddr=My+Location&daddr=43.12345,-76.12345
	} else if (evt.clicksource == "rightPane" || evt.clicksource == "title" || evt.clicksource == "infoWindow" || evt.clicksource == "subtitle") {

		var truckData = {
			previousWindow : "truckLocator",
			truckDescription : evt.annotation.truckDescription,
			truckName : evt.title,
			truckAddress : evt.subtitle,
			truckImage : evt.annotation.truckImage,
			truckId : evt.annotation.truckid,
			phone : evt.annotation.phone,
		};
		Alloy.Globals.dashboard.addView(Alloy.createController('foodTruckDetail', truckData).getView());
		Ti.App.Properties.setString("openWindow", "foodTruckDetail");
	} else if (evt.clicksource == "circle") {
	}
});

// $.anotationView.addEventListener('click', function(e){
//
// });

setInterval(function() {
	$.mapview.addEventListener('regionchanged', setMapdata);
}, 3000);

$.mapview.addEventListener('regionchanged', setMapdata);
function setMapdata(evt) {

	//setTimeout(function(){
	$.mapview.removeEventListener('regionchanged', setMapdata);
	//alert(evt.latitude+'='+evt.longitude);
	var newCoordsData1 = {};
	newCoordsData1 = {
		latitude : evt.latitude,
		longitude : evt.longitude
	};

	//alert(_.size(newCoordsData1));

	if (_.size(newCoordsData1) > 0) {
		if (eventFlag == false) {
			//$.anotationView.visible = false;
			showAnnotation(newCoordsData1);
		}
	}

	fetchAddress(evt.latitude, evt.longitude);
	// var url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + evt.latitude + "," + evt.longitude + "&sensor=true";
	// var client = Ti.Network.createHTTPClient({
	// // function called when the response data is available
	// onload : function(e) {
	// //Ti.API.info("Received text: " + this.responseText);
	// //alert('success');
	// var addressData = JSON.parse(this.responseText);
	// try {
	// if (!commonFile.isNullVal(addressData.results[0].formatted_address)) {
	// $.address.text = addressData.results[0].formatted_address;
	// } else {
	// $.address.text = "Getting address...";
	// }
	// } catch(ex) {
	// $.address.text = "Getting address...";
	// }
	//
	// },
	// // function called when an error occurs, including a timeout
	// onerror : function(e) {
	// Ti.API.debug(e.error);
	// //alert('error');
	// },
	// timeout : 5000 // in milliseconds
	// });
	// // Prepare the connection.
	// client.open("GET", url);
	// // Send the request.
	// client.send();

};

function fetchAddress(latData, longData) {

	var url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + latData + "," + longData + "&sensor=true";
	var client = Ti.Network.createHTTPClient({
		// function called when the response data is available
		onload : function(e) {
			//Ti.API.info("Received text: " + this.responseText);
			//alert('success');
			var addressData = JSON.parse(this.responseText);
			try {
				if (!commonFile.isNullVal(addressData.results[0].formatted_address)) {
					$.address.text = addressData.results[0].formatted_address;
				} else {
					$.address.text = "Getting address...";
				}
			} catch(ex) {
				$.address.text = "Getting address...";
			}

		},
		// function called when an error occurs, including a timeout
		onerror : function(e) {
			Ti.API.debug(e.error);
			//alert('error');
		},
		timeout : 5000 // in milliseconds
	});
	// Prepare the connection.
	client.open("GET", url);
	// Send the request.
	client.send();
}

$.mapview.addEventListener('complete', function(evt) {
	//$.pin.opacity = 1;

	// $.pin.animate({
	// opacity : 1,
	// duration : 7000
	// });

	// $.truckLocatorSuperView.animate({
	// opacity : 1,
	// duration : 7000
	// });

});

// mapview.addEventListener('pinchangedragstate', function(evt) {
// alert("pinchangedragstate");
// });
//
// mapview.addEventListener('onsnapshotready', function(evt) {
// alert("onsnapshotready");
// });
//
// mapview.addEventListener('regionwillchange', function(evt) {
// alert("regionwillchange");
// });
//
// mapview.addEventListener('focus', function(evt) {
// alert("focus");
// });

$.navigate.addEventListener('click', function(e) {
	$.mapview.region = {
		latitude : userCurrentLocation.latitude,
		longitude : userCurrentLocation.longitude,
		latitudeDelta : 0.05,
		longitudeDelta : 0.05
	};
});

function geolocation() {
	var hasLocationPermissions = Ti.Geolocation.hasLocationPermissions(Ti.Geolocation.AUTHORIZATION_ALWAYS);

	if (hasLocationPermissions) {
		//alert('hasLocationPermissions = '+hasLocationPermissions);
		//	if (parseInt(Titanium.Platform.version) >= 5) {
		//	loadMap();// temp
		coords = commonFile.getCurrentLocation($.truckLocatorSuperView);
		Ti.API.info('coord : ' + JSON.stringify(coords));
		
		//alert("1 == "+JSON.stringify(coords));
		if (coords != null) {
			userCurrentLocation = {
				latitude : coords.latitude,
				longitude : coords.longitude,
			}
			$.mapview.region = {
				latitude : coords.latitude,
				longitude : coords.longitude,
				latitudeDelta : 0.05,
				longitudeDelta : 0.05
			};
			showAnnotation(coords);
		}else{
			//temp
			// coords = {
				// latitude : 19.4690,
				// longitude : 72.8182
			// }
			// userCurrentLocation = {
				// latitude : coords.latitude,
				// longitude : coords.longitude,
			// }
			// $.mapview.region = {
				// latitude : coords.latitude,
				// longitude : coords.longitude,
				// latitudeDelta : 0.05,
				// longitudeDelta : 0.05
			// };
			// showAnnotation(coords);
			//end
			alert('Please enable location service');
		}
 
		//showAnnotation(coords);
		//temp comment
		//	}
	}

	//Ti.Media.requestCameraPermissions();

	Ti.Geolocation.requestLocationPermissions(Ti.Geolocation.AUTHORIZATION_ALWAYS, function(e) {
		if (e.success) {
			//Ti.Media.requestCameraPermissions();
			Ti.Media.requestCameraPermissions(function(e) {
				if (e.success) {
					Ti.API.info('success');
					//	loadMap(); temp

					coords = commonFile.getCurrentLocation($.truckLocatorSuperView);
					Ti.API.info('coords : ' + JSON.stringify(coords));
					//alert("2 == "+JSON.stringify(coords));

					if (coords != null) {
						userCurrentLocation = {
							latitude : coords.latitude,
							longitude : coords.longitude,
						}

						$.mapview.region = {
							latitude : coords.latitude,
							longitude : coords.longitude,
							latitudeDelta : 0.05,
							longitudeDelta : 0.05
						};
						showAnnotation(coords);

					}else{
						Ti.API.info('inside else');
						alert('Please enable location service');
					}
				}
			});
			// setTimeout(function(e) {
			// //	loadMap();
			// coords = commonFile.getCurrentLocation($.truckLocatorSuperView);
			// Ti.API.info('coord : ' + JSON.stringify(coords));
			// //alert("3 == "+JSON.stringify(coords));
			//
			// if (coords != null) {
			// showAnnotation(coords);
			//
			// }
			// //temp comment
			// }, 1500);
			//loadMap(); temp

		} else {
			Ti.Media.requestCameraPermissions();
		}
	});

	Ti.Media.requestCameraPermissions(function(e) {
		if (e.success) {
			Ti.API.info('success');
			// loadMap();
			//
			// coords = commonFile.getCurrentLocation($.truckLocatorSuperView);
			// Ti.API.info('coord : ' + JSON.stringify(coords));
			// alert(JSON.stringify(coords));
			//
			// if (coords != null) {
			// showAnnotation(coords);
			//
			// }
		}
	});
}

//Ti.API.info('Titanium.Platform.version = '+Titanium.Platform.version);

// function openDetail(){
// // Alloy.Globals.dashboard.addView(Alloy.createController('foodTruckDetail').getView());
// // Ti.App.Properties.setString("openWindow", "foodTruckDetail");
//
// Alloy.Globals.dashboard.addView(Alloy.createController('yourCart').getView());
// Ti.App.Properties.setString("openWindow", "yourCart");
//
// }
