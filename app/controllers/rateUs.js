// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

Alloy.Globals.rateUs = null;
Alloy.Globals.rateUs = $;

/* Add require files */
var commonFile = require('common');


commonFile.createTouchEffect($.dislike,Ti.App.Properties.getString('appPrimaryColor')||"#CB202D","#333333");
commonFile.createTouchEffect($.like,Ti.App.Properties.getString('appPrimaryColor')||"#CB202D","#333333");


$.close.text = commonFile.closeIcon;
$.dislike.text = commonFile.dislikeIcon;
$.like.text = commonFile.likeIcon;
/*
 * clear memory on back
 */
function goToBack() {
	$.rateUs.animate({
		opacity : 0,
		duration : 200
	}, function() {
		setTimeout(function() {
			Ti.App.Properties.setString("openWindow", args.previousWindow);
			Alloy.Globals.rateUs = null;
			commonFile = null;
			$.rateUs.removeAllChildren();
			$.rateUs.parent.remove($.rateUs);

		}, 201);
	});
};

exports.goToBack = goToBack;

$.rateUs.animate({
	opacity : 1,
	duration : 200
});

$.close.addEventListener('click',exports.goToBack);
