// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

Alloy.Globals.signIn = null;
Alloy.Globals.signIn = $;

var userId,userName,userPhone,userAccessToken = null;
/* Add require files */
var commonFile = require('common');

commonFile.createTouchEffect($.skip, "#a6ffffff", "#ffffff");

commonFile.createTouchEffect($.signIn, "#a6ffffff", "#ffffff");


$.signInLbl.backgroundColor = Ti.App.Properties.getString('appPrimaryColor')||"#CB202D";
//$.close.text = commonFile.closeIcon;
/*
 * clear memory on back
 */
function goToBack() {
	$.signIn.animate({
		opacity : 0,
		duration : 200
	}, function() {
		setTimeout(function() {
			Ti.App.Properties.setString("openWindow", args.previousWindow);
			Alloy.Globals.signIn = null;
			commonFile = null;
			args = null;
			$.signIn.removeAllChildren();
			$.signIn.parent.remove($.signIn);

		}, 201);
	});
};

exports.goToBack = goToBack;

$.signIn.animate({
	opacity : 1,
	duration : 200
});

var data = [$.nameTxt, $.numberTxt];
$.signIn.addEventListener('click', function(e) {
	commonFile.blurTextfield(e, data);
});
// $.facebookIcon.text = commonFile.facebookIcon;
// $.googleIcon.text = commonFile.googleIcon;
//
// $.close.addEventListener('click',exports.goToBack);
//
$.signInLbl.addEventListener('click', function(e) {

	if ($.nameTxt.getValue() == null || $.nameTxt.getValue() == "") {
		commonFile.showAlert($.signInSuperView,"Please enter your name.");
	} else if ($.numberTxt.getValue() == null || $.numberTxt.getValue() == "") {
		commonFile.showAlert($.signInSuperView,"Please enter your phone no.");
	} else {

		commonFile.showFullLoader($.signInSuperView);
		var url = commonFile.loginApi;
		var params = {};
		var params = {
			name : $.nameTxt.getValue(),
			mobile : $.numberTxt.getValue(),
			country_code : 91
		};

		commonFile.webServiceCall(url, JSON.stringify(params), loginCallBack, "POST", $.signInSuperView);
	}

	
});
  
function loginCallBack(response){
	Ti.API.info('response = ' + JSON.stringify(response));
	if (response.status == true) {
		var windowData = {
			previousWindow : "signIn",
			otp : response.data.otp,
			phone : $.numberTxt.getValue(),
			name : $.nameTxt.getValue(),
			callback : saveUser
		};
		userId = response.data.id;
		userName = $.nameTxt.getValue();
		userPhone = $.numberTxt.getValue();
	//	userAccessToken = response.response[0].user_access_token;
		Alloy.Globals.dashboard.addView(Alloy.createController('verification',windowData).getView());
		Ti.App.Properties.setString("openWindow", "verification");
		commonFile.hideFullLoader($.signInSuperView);
	} else {
		commonFile.hideFullLoader($.signInSuperView);
		commonFile.showAlert($.signInSuperView,"Something went wrong");
	}
}

function saveUser(){
	Ti.App.Properties.setString("userId",userId);
	Ti.App.Properties.setString("userName",userName);
	Ti.App.Properties.setString("userPhone",userPhone);
//	Ti.App.Properties.setString("accessToken",userAccessToken);
}

$.skip.addEventListener('click', exports.goToBack); 