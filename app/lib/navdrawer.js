/*
 * create navigation drawer 
 */
var commonFile = require('common');
var TiDrawerLayout = null;
var drawer = null;
var contentView = null;
var drawerWidth = null;
var leftSlider = null;
var centerContainerView = null;
exports.createDrawer = function(centerView, data, leftdrawer, leftdrawerData) {
 
    TiDrawerLayout = require('com.tripvi.drawerlayout');
    
      centerContainerView = Ti.UI.createView({
        width : Titanium.UI.FILL,
        height : Titanium.UI.FILL,
        top : "0dp",
        left:"0dp"
    });
    
    
    //contentView = Alloy.createController(centerView, data).getView();
    centerContainerView.add(Alloy.createController(centerView, data).getView());
    drawerWidth = ((commonFile.platformWidth * 80) / 100);
    leftSlider = Alloy.createController(leftdrawer, leftdrawerData).getView();

    drawer = TiDrawerLayout.createDrawer({
        leftView : leftSlider,
        centerView : centerContainerView,
        leftDrawerWidth : drawerWidth,
        hideToolbar : true
    });
    
    return drawer;

};



exports.setCenterView = function(windowName) {
	
	var abc = centerContainerView.add(windowName);
    drawer.centerView = abc;
};

exports.toggleEffect = function() {
    drawer.toggleLeftWindow();
};

exports.removeWindow = function(windowName) {
   centerContainerView.remove(windowName);
};
