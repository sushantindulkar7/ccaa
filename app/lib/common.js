/* Set Header height*/
if (Ti.Platform.osname == "android") {
	exports.headerHeight = "45dp";
} else if (Ti.Platform.osname == "iphone") {
	exports.headerHeight = "65dp";
}

var apiCallFlag = true;
/* Set Dynamic Platform Width*/
exports.platformWidth = Ti.Platform.displayCaps.platformWidth / Ti.Platform.displayCaps.logicalDensityFactor;

/* Set Dynamic Container Width*/
exports.containerWidth = parseInt(exports.platformWidth - 30);

/* Font Icon */
exports.settingIcon = "\ue900";
exports.linkIcon = "\ue901";
exports.cartIcon = "\ue902";
exports.truckIcon = "\ue903";
exports.locationIcon = "\ue904";
exports.googleSquareIcon = "\ue905";
exports.googleIcon = "\ue906";
exports.facebookSquareIcon = "\ue907";
exports.facebookIcon = "\ue908";
exports.shareIcon = "\ue909";
exports.alarmIcon = "\ue90a";
exports.starIcon = "\ue90b";
exports.closeCircleIcon = "\ue90c";
exports.closeIcon = "\ue90d";
exports.minusCircleIcon = "\ue90e";
exports.minusIcon = "\ue90f";
exports.plusCircleIcon = "\ue910";
exports.plusIcon = "\ue911";
exports.arrowDownIcon = "\ue912";
exports.arrowUpIcon = "\ue913";
exports.arrowRightIcon = "\ue914";
exports.arrowLeftIcon = "\ue915";
exports.menuIcon = "\ue916";
exports.phoneIcon = "\ue917";
exports.homeIcon = "\ue918";
exports.truck2Icon = "\ue919";
exports.clockIcon = "\ue91a";
exports.likeIcon = "\ue91b";
exports.dislikeIcon = "\ue91c";
exports.searchIcon = "\ue91d";
exports.noteIcon = "\ue91e";
exports.stopwatchIcon = "\ue920";
exports.thumbsDownIcon = "\ue921";
exports.thumbsUpIcon = "\ue922";
exports.googlePlusIcon = "\ue923";
exports.verticalDotsIcon = "\ue924";
exports.horizontalDotsIcon = "\ue925";
exports.downIcon = "\ue926";
exports.upIcon = "\ue927";
exports.rightIcon = "\ue928";
exports.googleSquare2Icon = "\ue92a";
exports.rupeeIcon = "\ue92b";
exports.dollarIcon = "\ue92c";
exports.circleIcon = "\ue92d";
exports.bellIcon = "\ue92e";
exports.userIcon = "\ue92f";
exports.calendarIcon = "\ue930";

/* Touch Effect */
// sample : touchEffect.createTouchEffect($.label, "#a6ffffff", "#ffffff");
exports.createTouchEffect = function(vwToAddEffect, colorForAnimation, originalColor) {

	vwToAddEffect.removeEventListener('touchstart', _touchStart);
	vwToAddEffect.removeEventListener('touchend', _touchEnd);
	vwToAddEffect.removeEventListener('touchcancel', _touchCancel);

	vwToAddEffect.addEventListener('touchstart', _touchStart);
	vwToAddEffect.addEventListener('touchend', _touchEnd);
	vwToAddEffect.addEventListener('touchcancel', _touchCancel);

	function _touchStart(e) {
		e.source.color = colorForAnimation !== undefined ? colorForAnimation : originalColor;
		//if you want to change background color of the label then use backgroundColor instead of color
	}

	function _touchEnd(e) {
		e.source.color = originalColor !== undefined ? originalColor : originalColor;
	}

	function _touchCancel(e) {
		e.source.color = originalColor !== undefined ? originalColor : originalColor;
	}

};

/* color code */
exports.white = "#ffffff";
exports.whiteE = "#a6ffffff";
exports.headerColor = Ti.App.Properties.getString('appPrimaryColor') ||"#CB202D";
exports.buttonTextColor = "#ffffff";
exports.leftSliderColor = "#fff";
exports.leftSliderMenuColor = Ti.App.Properties.getString('sliderColor') || "#333333";
exports.textColor1 = "#333333";
exports.textColor2 = "#C6B548";
exports.buttonColor1 = "#59595B";
exports.buttonColor2 = "#521352";
exports.txtBorderColor = "#808080";

/* Check Null Value*/
exports.isNullVal = function(val) {
	if (val === undefined || val == null || val == "" || _.isEmpty(val))
		return true;
	else
		return false;
};

/* Toast NOtification */
/*
 exports.toast = Ti.UI.createLabel({
 width : "200dp",
 height : "50dp",
 borderRadius : 5,
 textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER,
 font : {
 fontSize : "14dp"
 },
 zIndex : 100,
 color : "#ffffff",
 backgroundColor : "#73000000",
 top : "70%",
 });

 exports.showAlert = function(currentWindow, msg) {

 exports.toast.setText(msg);

 currentWindow.add(exports.toast);

 setTimeout(function() {
 currentWindow.remove(exports.toast);
 }, 1500);
 };
 */
var toastMessage = Ti.UI.createLabel({
	width : Ti.UI.FILL, //"200dp",
	height : "50dp",
	text : "Test Toast",
	//borderRadius : 5,
	textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER,
	font : {
		fontFamily : "roboto_medium",
		fontSize : "14dp"
	},
	zIndex : 2000,
	color : "#333333",
	//backgroundColor : "#73000000",
	backgroundColor : "#bfbfbf",
	//top : "70%",
	bottom : "-50dp"
});

exports.showAlert = function(currentWindow, msg) {

	toastMessage.setText(msg);

	currentWindow.add(toastMessage);
	toastMessage.animate({
		bottom : "0dp",
		duration : 200
	});
	setTimeout(function() {
		currentWindow.remove(toastMessage);
		toastMessage.bottom = "-50dp";
	}, 3000);

};

/* Window Open/Close function */
exports.navWindowObject = new Array();
exports.navWindowObjectId = new Array();
/**
 * @name : addWindowInNav
 * @description : Adding windows in object
 */
exports.addWindowInNav = function(windowId, data) {
	if (exports.navWindowObjectId[(exports.navWindowObjectId.length - 1)] != windowId || exports.navWindowObjectId.length == 0) {
		var win = Alloy.createController(windowId, data).getView();
		exports.navWindowObjectId.push(windowId);
		exports.navWindowObject.push(win);
		win.open();
	}
};

/**
 * @name : destroyWindowInNav
 * @description : Closed all windows in object
 */
exports.destroyWindowInNav = function() {

	for (var i = exports.navWindowObject.length - 1; i >= 0; i--) {
		Ti.API.info('Alloy.Globals.navWindowObject[' + i + '] = ' + exports.navWindowObject[i]);
		exports.navWindowObject[i].close();
	}
};

/**
 * @name : Alloy.Globals.popWindowInNav
 */
exports.popWindowInNav = function() {
	exports.navWindowObjectId.pop();
	exports.navWindowObject.pop();
};

/* Validation*/
exports.RequiredFieldValidatorDropDown = function(pck) {
	var isValid = false;
	if (pck.getSelectedRow(0).title.toString().indexOf('Select') === -1) {
		isValid = true;
	}

	return isValid;
};

exports.RequiredFieldValidatorTextBox = function(txt) {
	var isValid = false;
	if (txt.value !== null && txt.value !== undefined) {
		if (txt.value.trim() !== '') {
			isValid = true;
		}
	}
	return isValid;
};

exports.RegularExpressionName = function(txt) {
	var isValid = false;
	var reg = /^[A-Za-z ]+$/g;
	//var reg = /^([A-Za-z ]+) {2,20}$/;

	if (txt.value !== '' && txt.value !== null) {
		if (txt.value.trim() !== '') {

			if (txt.value.length >= 2 && txt.value.length <= 20) {

				if (!txt.value.match(reg)) {
					isValid = false;

				} else {
					isValid = true;
				}

			} else {
				isValid = false;

			}

		}
	} else {

		isValid = false;
	}
	return isValid;
};

exports.RegularExpressionNumber = function(txt) {

	var isValid = false;

	if (isNaN(txt.value) !== true && txt.value !== null) {

		isValid = true;
	}

	return isValid;
};

exports.RegularExpressionMobileNumber = function(txt) {
	var isValid = false;

	if (isNaN(txt.value) === false && txt.value !== null) {
		if (txt.value.length === 10) {
			if (txt.value.trim() !== '') {
				isValid = true;
			}
		}
	}

	return isValid;
};

exports.RegularExpressionNumberPin = function(txt) {
	var isValid = false;
	var val = txt.value;

	if (val === '' || isNaN(val) || val.length < 6)
		isValid = false;
	else
		isValid = true;

	return isValid;
};

exports.CheckForSpecialChars = function(str) {
	var iChars = "!@#$%^&*()+=-[]\\\';,./{}|\":<>?";
	for (var i = 0; i < str.length; i++) {
		if (iChars.indexOf(str.charAt(i)) != -1) {

			return false;
		}
	}
	return true;
};

exports.RegularExpressionWebSideName = function(str) {
	var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
	'((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
	'((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
	'(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
	'(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
	'(\\#[-a-z\\d_]*)?$', 'i');
	// fragment locater

	if (!pattern.test(str)) {
		return false;
	} else {
		return true;
	}
};

exports.RegularExpressionEmail = function(txt) {
	var isValid = false;
	//var reg = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

	var reg = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;

	if (txt.value !== '' && txt.value !== null) {

		if (reg.test(txt.value) === false) {
			isValid = false;

		} else {
			isValid = true;
		}
	} else {
		isValid = false;
	}
	return isValid;
};

exports.RegularExpressionEmailMultiple = function(txt) {
	var isValid = false;
	//var reg = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

	var reg = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;

	if (txt !== '' && txt !== null) {

		if (reg.test(txt) === false) {
			isValid = false;

		} else {
			isValid = true;
		}
	} else {
		isValid = false;
	}
	return isValid;
};

exports.RegularExpressionNewsletterEmail = function(txt) {
	var isValid = false;
	var reg = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

	if (txt.value.trim() !== '') {
		if (reg.test(txt.value) === false) {
			isValid = false;
		} else {
			isValid = true;
		}
	}
	return isValid;
};

exports.RegularExpressionUserName = function(txt) {
	var isValid = false;
	var reg = /^[A-Za-z0-9 ]+$/g;

	if (txt.value !== '' && txt.value.trim() !== '') {
		if (reg.test(txt.value) == false) {
			isValid = false;
		} else {
			isValid = true;
		}
	} else {
		isValid = false;
	}
	return isValid;
};

exports.RegularExpressionPassword = function(txt) {

	/*TODO need to make changes here*/
	var isValid = false;

	var str = txt.value.toString().trim();
	// var reg = /^([a-z0-9])|(?=.*[_$@.])+$/i;

	var letter = /[a-zA-Z]/;
	var number = /[0-9]/;
	//var valid = number.test(str) && letter.test(str);

	if (str.length >= 7) {
		// isValid = false;
		var _valid = number.test(str) && letter.test(str);
		// if (reg.test(str) == false) {
		if (_valid == false) {
			isValid = false;
		} else {
			isValid = true;
		}
	} else {
		isValid = false;
	}

	return isValid;
};

exports.RegularExpressionPasswordLength = function(txt) {
	var isValid = false;
	//Ti.API.info(JSON.stringify(txt));
	if (txt.value !== null) {
		if (txt.value.length >= 6) {
			if (txt.value.trim() !== '') {
				isValid = true;
			}
		}
	}
	return isValid;
};

exports.RegularExpressionURL = function(txt) {
	var isValid = false;
	var reg = /^(ftp|http|https):\/\/[^ "]+$/;

	if (txt.value !== '' && txt.value.trim() !== '') {
		if (reg.test(txt.value) == false) {
			isValid = false;
		} else {
			isValid = true;
		}
	} else {
		isValid = false;
	}
	return isValid;

};

exports.RegularExpressionEmail_Mobile = function(txt) {
	var isValid = false;

	//    var reg = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
	var reg = /^([_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,5}))|(\d+$)$/;

	if (txt.value !== '' && txt.value !== null) {
		if (reg.test(txt.value) === false) {
			isValid = false;
		} else {
			isValid = true;
		}
	} else {
		isValid = false;
	}
	return isValid;
};

/* Blur TextField*/
exports.blurTextfield = function(e, data) {

	if (e.source.type != "textfield") {
		if (Ti.Platform.osname === "android") {
			Ti.UI.Android.hideSoftKeyboard();
		} else {
			if (Ti.App.keyboardVisible) {
				_.each(data, function(value, k) {
					value.blur();
				});
			}
		}
	}
};

/* get location details */

exports.getCurrentLocation = function(windowName) {
	var coord = null;
	Ti.API.info('getting current geo location');
	if (Ti.Geolocation.locationServicesEnabled) {

		if (Ti.Platform.osname === "android") {

			Ti.Geolocation.purpose = "Receive User Location";
			Titanium.Geolocation.getCurrentPosition(function(e) {

				if (!e.success || e.error) {
					return;
				}

				coord = e.coords;
				var longitude = e.coords.longitude;
				var latitude = e.coords.latitude;

				return coord;

			});

		} else {
			//ios

		}
	} else {
		exports.showAlert(windowName, 'Please enable location service');
		// var intent = Ti.Android.createIntent({
		// action: 'android.settings.APPLICATION_SETTINGS',
		// });
		// intent.addFlags(Ti.Android.FLAG_ACTIVITY_NEW_TASK);
		// Ti.Android.currentActivity.startActivity(intent);
	}
	return coord;
};

//slider loader
var sliderImage = [];
for (var i = 1; i <= 12; i++) {
	sliderImage.push('/images/loader' + i + '.png');
}

// for (var i = 1; i <= 4; i++) {
// sliderImage.push('/images/truck_loader' + i + '.jpg');
// }

var sliderImageView = Titanium.UI.createImageView({
	images : sliderImage,
	width : '60dp',
	height : "60dp",
	// width:"150",
	// height:"100",
	repeatCount : 0,
	duration : 50,
	zIndex : 100,
	// top:"10dp",
	//borderColor:"red"
});

var loadingLbl = Titanium.UI.createLabel({
	text : "Loading in. Please wait...",
	color : "#000000",
	top : "5dp",
	font : {
		fontSize : "14dp",
		fontFamily : "roboto_medium"
	},
	height : "20dp",
});

var vwFullSubLoaderContainer = Ti.UI.createView({
	zIndex : 200,
	backgroundColor : "#ffffff",
	width : "80%", //"105dp",
	height : "105dp",
	borderRadius : "5",
	layout : "vertical"
});
vwFullSubLoaderContainer.add(sliderImageView);
vwFullSubLoaderContainer.add(loadingLbl);

var vwFullLoaderContainer = Ti.UI.createView({
	zIndex : 200,
	backgroundColor : "#a6000000",
	top : "0dp",
	left : "0dp",
	width : Ti.UI.FILL,
	height : Ti.UI.FILL
});

//vwFullLoaderContainer.add(sliderImageView);
//vwFullLoaderContainer.add(vwFullSubLoaderContainer);

vwFullLoaderContainer.add(sliderImageView);

exports.showFullLoader = function(currentWindow) {
	currentWindow.add(sliderImageView);
	sliderImageView.start();
};
exports.hideFullLoader = function(currentWindow) {
	// var _parent = vwFullLoaderContainer.parent;
	// if (_parent != null)
	// _parent.remove(sliderImageView);

	currentWindow.remove(sliderImageView);
};

//exports.baseUrl = "http://foodtruck.sudikmaharana.com/foodtruck/";
//exports.baseUrl = "http://foodtruck.sudikmaharana.in/foodtruck/";

exports.baseUrl = "http://app.sudikmaharana.in/api/v1/";

exports.gpsApi = "gettrucks";
exports.loginApi = "customer";
exports.truckInfoApi = "gettruckdetail";
exports.otpApi = "verifycustomerotp";
exports.addCartApi = "order/addtocart";
exports.cartListApi = "order/showcart";
exports.placeOrderApi = "order/placeorder";
exports.orderListApi = "order/pastordersbycustomer/";
exports.orderDetail = "order/orderdetail/";

/**
 * @name webServiceCall
 * @param string url
 * @param {Object} successevent_name
 * @param string xhrMethod
 * @param {Object} currentWindowName
 * @description : HTTP POST request is sent from this function.
 */
exports.webServiceCall = function(url, requestParam, successevent_name, xhrMethod, currentWindowName) {
	//alert("call");
	//function webServiceCall(requestMethod, strCallParams, successevent_name, errorevent_name, xhrMethod) {
	//var method = xhrMethod || "POST";
	//alert("1"+JSON.stringify(requestParam));
	//alert("requestParam = "+requestParam);
	//alert("requestParam = "+JSON.strequestParam);
	if (Titanium.Network.networkType === Titanium.Network.NETWORK_NONE) {
		//hideLoader();
		//	hideFullLoader(currentWindowName);
		var connectionContainer = Titanium.UI.createView({
			height : Ti.UI.FILL,
			width : Ti.UI.FILL,
			backgroundColor : "white",
			zIndex : 11
		});
		currentWindowName.add(connectionContainer);
		var subContainer = Titanium.UI.createView({
			height : Ti.UI.SIZE,
			width : Ti.UI.SIZE,
			layout : "vertical"
		});
		connectionContainer.add(subContainer);
		var connectionIcon = Titanium.UI.createLabel({
			top : "0dp",
			font : {
				fontSize : "130dp",
				fontFamily : "icomoon"
			},
			color : "#a6333333",
			text : exports.menuIcon
		});
		subContainer.add(connectionIcon);
		var noConnectionLbl = Titanium.UI.createLabel({
			top : "10dp",
			font : {
				fontSize : "18dp",
				fontFamily : "roboto_medium"
			},
			color : "#333333",
			text : "No Connection"
		});
		subContainer.add(noConnectionLbl);
		var checkConnectionLbl = Titanium.UI.createLabel({
			top : "7dp",
			font : {
				fontSize : "18dp",
				fontFamily : "Roboto-Regular"
			},
			color : "#333333",
			text : "Please check your internet connection"
		});
		subContainer.add(checkConnectionLbl);
		var refreshLbl = Titanium.UI.createLabel({
			height : "40dp",
			width : "40%",
			color : "#ffffff",
			backgroundColor : "#4d000000",
			top : "10dp",
			font : {
				fontSize : "13dp",
				fontFamily : "roboto_medium"
			},
			textAlign : Titanium.UI.TEXT_ALIGNMENT_CENTER,
			text : "REFRESH"
		});
		subContainer.add(refreshLbl);

		//touchEffect.createTouchEffect(refreshLbl, "#a6ffffff", "#ffffff");
		refreshLbl.addEventListener('click', function(e) {

			if (Titanium.Network.networkType === Titanium.Network.NETWORK_NONE) {

				subContainer.visible = false;
				setTimeout(function(e) {
					subContainer.visible = true;
				}, 100);
			} else {
				//alert("2"+JSON.stringify(requestParam));
				getWebserviceData(url, requestParam, successevent_name, xhrMethod, currentWindowName);
				currentWindowName.remove(connectionContainer);
				//  showLoader(currentWindowName);
			}

		});

	} else {
		//alert("3"+JSON.stringify(requestParam));
		getWebserviceData(url, requestParam, successevent_name, xhrMethod, currentWindowName);

	}
};

var getWebserviceData = function(url, requestParam, successevent_name, xhrMethod, currentWindowName) {
	//alert("4"+JSON.stringify(requestParam));
	var method = xhrMethod || "POST";
	var xhr = Titanium.Network.createHTTPClient({
		//timeout : 5000
		username : "admin",
		password : "1234"
	});
	xhr.onload = function() {

		try {
			var serviceResponse = JSON.parse(this.responseText);
			Ti.App.Properties.setBool("apiCallFlag", true);
		} catch(err) {
			Ti.API.info('catch error = ' + JSON.stringify(err.message));
			Ti.App.Properties.setBool("apiCallFlag", true);
		}
		if (serviceResponse !== null || serviceResponse !== undefined) {
			Ti.App.Properties.setBool("apiCallFlag", true);
			successevent_name(serviceResponse);
		}else{
			Ti.App.Properties.setBool("apiCallFlag", true);
		}

	};

	xhr.onerror = function(e) {
		Ti.API.info('into WS error');
		//hideFullLoader(currentWindowName);
		Ti.App.Properties.setBool("apiCallFlag", true);
		serviceResponse = {
			response : {
				listTrucks : {
					status : false
				}
			}
		};
		successevent_name(serviceResponse);
		var connectionContainer = Titanium.UI.createView({
			height : Ti.UI.FILL,
			width : Ti.UI.FILL,
			backgroundColor : "white",
			zIndex : 11
		});
		//currentWindowName.add(connectionContainer);
		var closeLbl = Titanium.UI.createLabel({
			top : "20dp",
			right : "20dp",
			font : {
				fontSize : "20dp",
				fontFamily : "icomoon"
			},
			color : "#333333",
			text : exports.closeIcon,
		});
		connectionContainer.add(closeLbl);
		var subContainer = Titanium.UI.createView({
			height : Ti.UI.SIZE,
			width : Ti.UI.SIZE,
			layout : "vertical"
		});
		connectionContainer.add(subContainer);
		var connectionIcon = Titanium.UI.createLabel({
			top : "0dp",
			font : {
				fontSize : "120dp",
				fontFamily : "icomoon"
			},
			color : "#a6333333",
			text : exports.menu
		});
		subContainer.add(connectionIcon);
		var noConnectionLbl = Titanium.UI.createLabel({
			top : "10dp",
			font : {
				fontSize : "18dp",
				fontFamily : "roboto_medium"
			},
			color : "#333333",
			text : "Can't Connect"
		});
		subContainer.add(noConnectionLbl);
		var checkConnectionLbl = Titanium.UI.createLabel({
			top : "7dp",
			font : {
				fontSize : "18dp",
				fontFamily : "Roboto-Regular"
			},
			color : "#333333",
			text : "Server Error"
		});
		subContainer.add(checkConnectionLbl);
		var tryAgain = Titanium.UI.createLabel({
			height : "40dp",
			width : "40%",
			color : "#ffffff",
			backgroundColor : "#4d000000",
			top : "10dp",
			font : {
				fontSize : "13dp",
				fontFamily : "roboto_medium"
			},
			textAlign : Titanium.UI.TEXT_ALIGNMENT_CENTER,
			text : "TRY AGAIN"
		});
		subContainer.add(tryAgain);
		//touchEffect.createTouchEffect(closeLbl, "#a6333333", "#333333");
		//touchEffect.createTouchEffect(tryAgain, "#a6ffffff", "#ffffff");

		tryAgain.addEventListener('click', function(e) {

			currentWindowName.remove(connectionContainer);
			//showLoader(currentWindowName);
			exports.webServiceCall(url, successevent_name, xhrMethod, currentWindowName);

		});
		closeLbl.addEventListener('click', function(e) {
			currentWindowName.remove(connectionContainer);
		});

	};
	Ti.API.info('baseUrl = ' + exports.baseUrl + url);
	Ti.API.info('requestParam = ' + requestParam);
	//	alert(requestParam);
	xhr.open(method, exports.baseUrl + url);
	//xhr.validatesSecureCertificate = true;
	xhr.setRequestHeader('Content-Type', 'application/json');
	// var access_token = Ti.App.Properties.getString("access_token") || "";
	// Ti.API.info('Ti.App.Properties.getString("access_token") = ' + Ti.App.Properties.getString("access_token"));
	// Ti.API.info('access token-->' + access_token);
	// xhr.setRequestHeader("Accesstoken", access_token);
	//xhr.open(method,"http://casite-716723.cloudaccess.net/?option=com_api&task=login.getdata&tmpl=component");

	//xhr.setRequestHeader('Content-Type', 'application/json');
	//Ti.API.info('Ti.App.Properties.getBool("apiCallFlag") = ' + Ti.App.Properties.getBool("apiCallFlag"));
	//alert(Ti.App.Properties.getBool("apiCallFlag"));
	//if (url == "gps") {
	//	//alert("inside1"+Ti.App.Properties.getBool("apiCallFlag"));
	//	if (Ti.App.Properties.getBool("apiCallFlag") == undefined || Ti.App.Properties.getBool("apiCallFlag") == true) {
			xhr.send(requestParam);
			// Ti.App.Properties.setBool("apiCallFlag", false);
		// }
	// } else {
		// xhr.send(requestParam);
	// }
};

