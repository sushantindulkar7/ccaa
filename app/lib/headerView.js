/*
 * create header view
 */
var commonFile = require('common');
var headerView,
    backArrow,
    userimage,
    wTitle,
    backicon,
    background_color,
    backWidth,
    backHeight,
    backBorderRadius,
    backLeft,
    headerImageView,
    cartLbl,
    orderLbl = null;
exports.createHeaderView = function(windowName, windowTitle, data, callback) {

	headerView = Ti.UI.createView({
		width : Titanium.UI.FILL,
		height : commonFile.headerHeight,
		top : "0dp",
		backgroundColor : commonFile.headerColor,
		//Ti.App.Properties.getString('appPrimaryColor') || "#CB202D",
	});

	exports.headerView = headerView;
	if (!commonFile.isNullVal(data)) {
		if (data.menu) {
			//display menu
			backicon = commonFile.menuIcon;
			background_color = "transparent";
			backWidth = "45dp";
			backHeight = "45dp";
			// backBorderRadius = "22.5dp";
			backLeft = "0dp";
			fontsize = "22dp";

		} else {
			// display back arrow
			backicon = commonFile.arrowLeftIcon;
			background_color = "transparent";
			backWidth = "45dp";
			backHeight = "45dp";
			backLeft = "0dp";
			fontsize = "22dp";
		}
	}

	backArrow = Ti.UI.createLabel({
		left : backLeft,
		width : backWidth,
		height : backHeight,
		borderRadius : backBorderRadius,
		font : {
			fontSize : fontsize,
			fontFamily : "icomoon",
		},
		backgroundColor : background_color,
		color : commonFile.white,
		textAlign : Titanium.UI.TEXT_ALIGNMENT_CENTER,
		text : backicon,
	});
	headerView.add(backArrow);

	wTitle = Ti.UI.createLabel({
		//left : "45dp",
		font : {
			fontSize : "15dp",
			fontFamily : "roboto_medium",
		},
		color : commonFile.white,
		text : windowTitle
	});
	headerView.add(wTitle);
	wTitle = null;

	userimage = Ti.UI.createImageView({
		right : "15dp",
		width : "34dp",
		height : "34dp",
		borderRadius : "17dp",
		borderColor : "green"
		//defaultImage : "/images/defaultUser.png"
	});
	//headerView.add(userimage);

	/// Cart Icon
	//if (windowName != "yourCart" && windowName != "orderOption" && windowName != "orderSummary" && windowName != "verification" && windowName != "pastOrder") {
	if (data.rightMenu) {
		cartLbl = Ti.UI.createLabel({
			right : "0dp",
			width : "45dp",
			height : "45dp",
			//borderColor:"white",
			font : {
				fontSize : fontsize,
				fontFamily : "icomoon",
			},
			backgroundColor : "transparent",
			color : commonFile.white,
			textAlign : Titanium.UI.TEXT_ALIGNMENT_CENTER,
			text : commonFile.cartIcon,
		});

		headerView.add(cartLbl);

		commonFile.createTouchEffect(cartLbl, "#a6ffffff", "#ffffff");

		cartLbl.addEventListener('click', function(e) {

			if (Ti.App.Properties.getString("userId") != null || Ti.App.Properties.getString("userId") != undefined) {
				
				var windowData = {
					previousWindow : windowName,
				};

				Alloy.Globals.dashboard.addView(Alloy.createController('yourCart', windowData).getView());
				Ti.App.Properties.setString("openWindow", "yourCart");
			} else {
				var windowData = {
					previousWindow : Ti.App.Properties.getString("openWindow"),
				};
				Alloy.Globals.dashboard.addView(Alloy.createController('signIn', windowData).getView());
				Ti.App.Properties.setString("openWindow", "signIn");
			}

		});

	} else if (windowName == "orderOption") {

		orderLbl = Ti.UI.createLabel({
			right : "0dp",
			width : "100dp",
			height : "45dp",
			//borderColor:"white",
			font : {
				fontSize : "13dp",
				fontFamily : "Roboto-Regular"
			},
			backgroundColor : "transparent",
			color : commonFile.white,
			textAlign : Titanium.UI.TEXT_ALIGNMENT_CENTER,
			text : "order details",
		});

		headerView.add(orderLbl);

		commonFile.createTouchEffect(orderLbl, "#a6ffffff", "#ffffff");

		orderLbl.addEventListener('click', function(e) {

			var windowData = {
				previousWindow : windowName,
			};
			Alloy.Globals.dashboard.addView(Alloy.createController('orderSummary', windowData).getView());
			Ti.App.Properties.setString("openWindow", "orderSummary");
		});

	}
	//end

	commonFile.createTouchEffect(backArrow, "#a6ffffff", "#ffffff");

	backArrow.addEventListener('click', function(e) {

		if (data.menu) {
			Ti.API.info('lcickc');
			var drawer = require('navdrawer').toggleEffect();
			drawer = null;
		} else {
			Ti.API.info('lcickc11');
			// require('navdrawer').removeWindow(windowObject);
			//close currentview / window

			//  windowName.parent.remove(windowName);

			return callback();
		}
	});

	return headerView;
};
